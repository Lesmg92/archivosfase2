#include "SistemaA.h"
#include "ListaDirectorios.h"

/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA FORMATEAR-------------------------*/
/*------------------------------------------------------------------------------------*/
int getNStructs(int partSize){
    unsigned int nstruct = (partSize - 2*sizeof(superBoot) - sizeof(bitmapArbolDirectorio) - sizeof(bitmapDetalleDirectorio) - sizeof(bitmapTablaINodos)- sizeof(bitmapBloques))/(sizeof(arbolVirtualDirectorio)+sizeof(detalleDirectorio)+sizeof(inodo)+sizeof(bloque));
    return nstruct;
}
    
void formatPart(char ruta[256],char name[16],int partSize,int startPart)
{
    printf("    ***********************************************************\n");
        superBoot n_sb;
        int numStructs = getNStructs(partSize);
            
            strcpy(n_sb.sb_nombre_hd,name);
            n_sb.sb_arbol_virtual_count= numStructs;
            n_sb.sb_detalle_directorio_count = numStructs;
            n_sb.sb_inodos_count=numStructs;
            n_sb.sb_bloques_count = numStructs;
            n_sb.sb_arbol_virtual_free=numStructs;
            n_sb.sb_detalle_directorio_free=numStructs;
            n_sb.sb_inodos_free=numStructs;
            n_sb.sb_bloques_free=numStructs;
            n_sb.sb_date_creacion = time(0);
            n_sb.sb_date_ultimo_montaje = time(0);
            n_sb.sb_montajes_count=1;
            n_sb.sb_ap_bitmap_arbol_directorio= startPart+sizeof(superBoot);
            n_sb.sb_ap_arbol_directorio= n_sb.sb_ap_bitmap_arbol_directorio + sizeof(bitmapArbolDirectorio);
            n_sb.sb_ap_bitmap_detalle_directorio = n_sb.sb_ap_arbol_directorio + sizeof(arbolVirtualDirectorio)*numStructs;
            n_sb.sb_ap_detalle_directorio = n_sb.sb_ap_bitmap_detalle_directorio + sizeof(bitmapDetalleDirectorio);
            n_sb.sb_ap_bitmap_tabla_inodo = n_sb.sb_ap_detalle_directorio + numStructs*sizeof(detalleDirectorio);
            n_sb.sb_ap_tabla_inodo = n_sb.sb_ap_bitmap_tabla_inodo + sizeof(bitmapTablaINodos);
            n_sb.sb_ap_bitmap_bloques =n_sb.sb_ap_tabla_inodo + sizeof(inodo)*numStructs;
            n_sb.sb_ap_bloques = n_sb.sb_ap_bitmap_bloques + sizeof(bitmapBloques);
            n_sb.sb_ap_log = n_sb.sb_ap_bloques + sizeof(bloque)*numStructs;
            n_sb.sb_size_struct_arbol_directorio = sizeof(arbolVirtualDirectorio);
            n_sb.sb_size_struct_detalle_directorio= sizeof(detalleDirectorio);
            n_sb.sb_size_struct_inodo = sizeof(inodo);
            n_sb.sb_size_struct_bloque = sizeof(bloque);
            n_sb.sb_first_free_bit_arbol_directorio =0;
            n_sb.sb_first_free_bit_detalle_directorio =0;
            n_sb.sb_first_free_bit_tabla_inodo=0;
            n_sb.sb_first_free_bit_bloque=0;
            n_sb.sb_magic_num=201314808;
        
        // ESCRIBIR CON EL NUEVO FORMATO
        
        setSuperBoot(ruta,n_sb,startPart);     
        
        int limit = limitStructs(numStructs);
        int i=0;
        
        // Arbol de directorio
        bitmapArbolDirectorio n_bm_arbol_directorio;
        for(i;i<limit;i++)
        {
          n_bm_arbol_directorio.bmp_arbol_directorio[i]='0';
        }
        set_bm_avd(ruta,startPart,n_bm_arbol_directorio);
        
        //--------------detalle de directorio
        i=0;
        bitmapDetalleDirectorio n_bm_detalle_directorio;
        for(i;i<limit;i++)
        {
          n_bm_detalle_directorio.bmp_detalle_directorio[i]='0';
        }
        set_bm_dd(ruta,startPart,n_bm_detalle_directorio);
        
        //---------------tabla de inodos
        i = 0;
        bitmapTablaINodos n_bm_inodos;
        for(i;i<limit;i++)
        {
          n_bm_inodos.bmp_tabla_inodos[i]='0';
        }
        set_bm_tabla_in(ruta,startPart,n_bm_inodos);
                
        //----------------------------datos
        i = 0;
       bitmapBloques nuevo_bmp_bloques;
       for (i;i<limit;i++) {
         nuevo_bmp_bloques.bmp_bloques[i]='0';
       }
       set_bm_bloques(ruta,startPart,nuevo_bmp_bloques);
        
        printf("        Formato realizado con exito ヽ(〃＾▽＾〃)ﾉ\n");
         
       /////// --> crear la carpeta raiz
        int by = new_AVD(ruta,startPart,"/");

        /////////////// -------------- pruebas agre        
      //  char conteni[300] = "este es el contenido muy largo de un string y espero que ocupe varios bloques para probar si me esta funcionando el cotenido de esta cosa fea 140";
      //  agregarArchivo(ruta,startPart,conteni,"archivo.txt",by);
      //  leerArchivo(ruta,startPart,"archivo.txt");
    
    printf("    ***********************************************************\n");
}

int limitStructs(int numStru)
{
    if(numStru>20000)
    {
        return 20000;
    }
    else
    {
        return numStru;
    }
}

/*------------------------------------------------------------------------------------*/
/*------------------------GET Y SET PARA STRUCTS Y BITMAPS----------------------------*/
/*------------------------------------------------------------------------------------*/

superBoot getSuperBoot(char ruta[300], int startParticion)
{
    superBoot n_sb;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,startParticion,SEEK_SET);
       fread(&n_sb,sizeof(superBoot),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
    return n_sb;
    
}

void setSuperBoot(char ruta[300],superBoot sb1,int posicion)
{
    FILE *aptrwrsb;
    aptrwrsb = fopen(ruta,"rb+");
    if(aptrwrsb!=NULL){ 
       fseek(aptrwrsb,posicion,SEEK_SET);
       fwrite(&sb1,sizeof(superBoot),1,aptrwrsb);
       fclose(aptrwrsb);
    }
    else
        printf("No se encontró el disco para escribir super boot\n"); 
}

arbolVirtualDirectorio getAVD(char ruta[300], int byte_avd)
{
    arbolVirtualDirectorio n_avd;
    FILE *archivoptra;
    archivoptra = fopen(ruta,"rb+");
    
    if(archivoptra!=NULL){ 
       fseek(archivoptra,byte_avd,SEEK_SET);
       fread(&n_avd,sizeof(arbolVirtualDirectorio),1,archivoptra);
       fclose(archivoptra);
       fflush(archivoptra);
    }
    return n_avd;
}

void setAVD(char ruta[300],int byte_avd,arbolVirtualDirectorio avdadd)
{
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_avd,SEEK_SET);
       fwrite(&avdadd,sizeof(arbolVirtualDirectorio),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
}

detalleDirectorio getDetalleDirectorio(char ruta[300], int byte_dd)
{
    detalleDirectorio n_dd;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_dd,SEEK_SET);
       fread(&n_dd,sizeof(detalleDirectorio),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
    return n_dd;
}

void setDetalleDirectorio(char ruta[300],int byte_dd,detalleDirectorio ddAdd)
{
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_dd,SEEK_SET);
       fwrite(&ddAdd,sizeof(detalleDirectorio),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
}

inodo getINodo(char ruta[300], int byte_inodo)
{
    inodo n_inodo;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_inodo,SEEK_SET);
       fread(&n_inodo,sizeof(inodo),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
    return n_inodo;
}

void setINodo(char ruta[300], int byte_inodo, inodo inodoAdd)
{
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_inodo,SEEK_SET);
       fwrite(&inodoAdd,sizeof(inodo),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
}

bloque getBloque(char ruta[300], int byte_bloque)
{
    bloque n_bloque;
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_bloque,SEEK_SET);
       fread(&n_bloque,sizeof(bloque),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
    return n_bloque;
}


void setBloque(char ruta[300], int byte_bloque,bloque bloqAdd)
{
    FILE *archivoptr;
    archivoptr = fopen(ruta,"rb+");
    
    if(archivoptr!=NULL){ 
       fseek(archivoptr,byte_bloque,SEEK_SET);
       fwrite(&bloqAdd,sizeof(bloque),1,archivoptr);
       fclose(archivoptr);
       fflush(archivoptr);
    }
}

bitmapArbolDirectorio get_bm_avd(char ruta[300], int startPart)
{
    bitmapArbolDirectorio bm_avd;
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_arbol_directorio,SEEK_SET);
       fread(&bm_avd,sizeof(bitmapArbolDirectorio),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para leer bitmap avd\n");
    return bm_avd;
}

void set_bm_avd(char ruta[300], int startPart, bitmapArbolDirectorio nadd)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_arbol_directorio,SEEK_SET);
       fwrite(&nadd,sizeof(bitmapArbolDirectorio),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para escribir bitmap avd\n");
}

bitmapDetalleDirectorio get_bm_dd(char ruta[300], int startPart)
{
    bitmapDetalleDirectorio bm_dd;
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_detalle_directorio,SEEK_SET);
       fread(&bm_dd,sizeof(bitmapDetalleDirectorio),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para leer bitmap detalle dir\n");
    return bm_dd;
}

void set_bm_dd(char ruta[300], int startPart, bitmapDetalleDirectorio nadd)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_detalle_directorio,SEEK_SET);
       fwrite(&nadd,sizeof(bitmapDetalleDirectorio),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para escribir bitmap detalle dir\n");
}

bitmapTablaINodos get_bm_tabla_in(char ruta[300], int startPart)
{
    bitmapTablaINodos bm_in;
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_tabla_inodo,SEEK_SET);
       fread(&bm_in,sizeof(bitmapTablaINodos),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para leer bitmap inodos\n");
    return bm_in;
}

void set_bm_tabla_in(char ruta[300], int startPart, bitmapTablaINodos nadd)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_tabla_inodo,SEEK_SET);
       fwrite(&nadd,sizeof(bitmapTablaINodos),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para escribir bitmap inodos\n");
}

bitmapBloques get_bm_bloques(char ruta[300], int startPart)
{
    bitmapBloques bm_b;
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_bloques,SEEK_SET);
       fread(&bm_b,sizeof(bitmapBloques),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para leer bitmap bloques\n");
    return bm_b;
}

void set_bm_bloques(char ruta[300], int startPart,bitmapBloques nadd)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    FILE *aptrwrbmad;
    aptrwrbmad = fopen(ruta,"rb+");
    if(aptrwrbmad!=NULL){ 
       fseek(aptrwrbmad,sb1.sb_ap_bitmap_bloques,SEEK_SET);
       fwrite(&nadd,sizeof(bitmapBloques),1,aptrwrbmad);
       fclose(aptrwrbmad);
    }
    else
        printf("No se encontró el disco para escribir bitmap bloques\n");
}
/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA INICIALIZAR-----------------------*/
/*------------------------------------------------------------------------------------*/

int new_AVD(char ruta[300], int startPart, char nombre[30])
{   
    superBoot sb1 = getSuperBoot(ruta,startPart);
    bitmapArbolDirectorio bmp_avd = get_bm_avd(ruta,startPart);
    bmp_avd.bmp_arbol_directorio[sb1.sb_first_free_bit_arbol_directorio]='1';
    set_bm_avd(ruta,startPart,bmp_avd);
    
    int ap_det = new_detalle_directorio(ruta,startPart);
    arbolVirtualDirectorio n_avd;
    n_avd.avd_fecha_creacion=time(0);
    strcpy(n_avd.avd_nombre_directorio,nombre);
    n_avd.avd_ap_array_subdirectorios[0]=0;
    n_avd.avd_ap_array_subdirectorios[1]=0;
    n_avd.avd_ap_array_subdirectorios[2]=0;
    n_avd.avd_ap_array_subdirectorios[3]=0;
    n_avd.avd_ap_array_subdirectorios[4]=0;
    n_avd.avd_ap_array_subdirectorios[5]=0;
    n_avd.avd_ap_detalle_directorio = ap_det;
    n_avd.avd_ap_arbol_virtual_directorio = -1;

    int first_free = sb1.sb_first_free_bit_arbol_directorio;
    int byte_avd = sb1.sb_ap_arbol_directorio + first_free * sizeof(arbolVirtualDirectorio);
    setAVD(ruta,byte_avd,n_avd);
    
    sb1 = getSuperBoot(ruta,startPart);
    
    sb1.sb_arbol_virtual_free--;
    sb1.sb_first_free_bit_arbol_directorio++;
    
    setSuperBoot(ruta,sb1,startPart);
    return byte_avd;
}

int new_detalle_directorio(char ruta[300],int startPart)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    bitmapDetalleDirectorio bmp_dd = get_bm_dd(ruta,startPart);
    bmp_dd.bmp_detalle_directorio[sb1.sb_first_free_bit_detalle_directorio]='1';
    set_bm_dd(ruta,startPart,bmp_dd);
     
    detalleDirectorio n_dd;
    n_dd.dd_ap_detalle_directorio = -1;
    n_dd.dd_archive_1.dd_file_ap_inodo = 0;
    n_dd.dd_archive_1.dd_file_date_creacion = time(0);
    n_dd.dd_archive_1.dd_file_date_modificacion = time(0);
    strcpy(n_dd.dd_archive_1.dd_file_nombre," ");
    n_dd.dd_archive_2.dd_file_ap_inodo = 0;
    n_dd.dd_archive_2.dd_file_date_creacion = time(0);
    n_dd.dd_archive_2.dd_file_date_modificacion = time(0);
    strcpy(n_dd.dd_archive_1.dd_file_nombre," ");
    n_dd.dd_archive_3.dd_file_ap_inodo = 0;
    n_dd.dd_archive_3.dd_file_date_creacion = time(0);
    n_dd.dd_archive_3.dd_file_date_modificacion = time(0);
    strcpy(n_dd.dd_archive_3.dd_file_nombre," ");
    n_dd.dd_archive_4.dd_file_ap_inodo = 0;
    n_dd.dd_archive_4.dd_file_date_creacion = time(0);
    n_dd.dd_archive_4.dd_file_date_modificacion = time(0);
    strcpy(n_dd.dd_archive_4.dd_file_nombre," ");
    n_dd.dd_archive_5.dd_file_ap_inodo = 0;
    n_dd.dd_archive_5.dd_file_date_creacion = time(0);
    n_dd.dd_archive_5.dd_file_date_modificacion = time(0);
    strcpy(n_dd.dd_archive_5.dd_file_nombre," ");
    
    int first_free = sb1.sb_first_free_bit_detalle_directorio;
    int byte_retorno = sb1.sb_ap_detalle_directorio + first_free*sizeof(detalleDirectorio);
       
    setDetalleDirectorio(ruta,byte_retorno,n_dd);
        
    sb1.sb_first_free_bit_detalle_directorio++;
    sb1.sb_detalle_directorio_free--;
    
    setSuperBoot(ruta, sb1,startPart);
    
    return byte_retorno;
}

int new_inodo(char ruta[300],int startPart)
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    bitmapTablaINodos bmp_in = get_bm_tabla_in(ruta,startPart);
    bmp_in.bmp_tabla_inodos[sb1.sb_first_free_bit_tabla_inodo]='1';
    set_bm_tabla_in(ruta,startPart,bmp_in);
    
    int byte_retorno = sb1.sb_ap_tabla_inodo + sb1.sb_first_free_bit_tabla_inodo*sizeof(inodo);
     
    inodo n_inodo;
    n_inodo.i_ap_indirecto = -1;
    n_inodo.i_count_bloques_asignados = 0;
    n_inodo.i_count_inodo = 0;
    n_inodo.i_id_proper = 0;
    n_inodo.i_size_archivo = 0;
    n_inodo.i_array_bloques[0] = 0;
    n_inodo.i_array_bloques[1] = 0;
    n_inodo.i_array_bloques[2] = 0;
    n_inodo.i_array_bloques[3] = 0;
    
    setINodo(ruta,byte_retorno,n_inodo);
    
    sb1 = getSuperBoot(ruta,startPart);
    
    sb1.sb_first_free_bit_tabla_inodo++;
    sb1.sb_inodos_free--;
    
    setSuperBoot(ruta, sb1,startPart);
    
    return byte_retorno;
}

int new_bloque(char ruta[300],int startPart, char conte[25])
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    bitmapBloques bmp_b = get_bm_bloques(ruta,startPart);
    bmp_b.bmp_bloques[sb1.sb_first_free_bit_bloque]='1';
    set_bm_bloques(ruta,startPart,bmp_b);
    
    bloque n_bloque;
    strcpy(n_bloque.dbdata,conte);
    
    sb1.sb_first_free_bit_bloque++;
    sb1.sb_bloques_free--;
    
    setSuperBoot(ruta, sb1,startPart);

    int byteRet = sb1.sb_ap_bloques + sb1.sb_first_free_bit_bloque*sizeof(bloque); 
    setBloque(ruta,byteRet,n_bloque);

    return byteRet;
}

/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA MODIFICAR-------------------------*/
/*------------------------------------------------------------------------------------*/

void agregarContEnBloques(int byte_in, char ruta[300], int startPart, char contenido[300], int cont_contenido, int cont_llenos)
{
    inodo new_inodo_p = getINodo(ruta,byte_in);
    int m = 0;
    do{
        if(cont_llenos < new_inodo_p.i_count_bloques_asignados)
        {
            char contenido_bloque[25];
            strcpy (contenido_bloque,"                         ");
            strncpy (contenido_bloque,contenido+cont_contenido,25);
            int byte_bloq = new_bloque(ruta,startPart,contenido_bloque);
            new_inodo_p.i_array_bloques[m] = byte_bloq;
            cont_contenido = cont_contenido +25;
            cont_llenos++;
        }
        m++;
    }
    while(m<4);  
    
    setINodo(ruta,byte_in,new_inodo_p);
    
    if(cont_llenos < new_inodo_p.i_count_bloques_asignados)
    { 
        int byte_n_in = new_inodo(ruta,startPart);
                // ---- actualizo con ptr ind
        new_inodo_p.i_ap_indirecto = byte_n_in; 
        setINodo(ruta, byte_in, new_inodo_p);
        
        int bloques_asignados = new_inodo_p.i_count_bloques_asignados;
        int count = new_inodo_p.i_count_inodo;
        count++;
        int id_proper = new_inodo_p.i_id_proper;
        int n_size = new_inodo_p.i_size_archivo;
        
        // ---- actualizo los datos en el nuevo creado
        inodo nuevo_indirecto = getINodo(ruta,byte_n_in);
        nuevo_indirecto.i_count_bloques_asignados = bloques_asignados;
        nuevo_indirecto.i_count_inodo = count;
        nuevo_indirecto.i_id_proper = id_proper;
        nuevo_indirecto.i_size_archivo = n_size;
        
        setINodo(ruta,byte_n_in,nuevo_indirecto);
        
        agregarContEnBloques(byte_n_in,ruta,startPart,contenido,cont_contenido,cont_llenos);
    }
}

void agregarArchivo(char ruta[300],int startPart, char contenido[300], char nombre[16], int from_avd_ap_dd)
{    
    int size_contenido = strlen(contenido);
    int bloques_asignados = size_contenido/25;
    
    int div_mod = size_contenido % 25;
    if(div_mod > 0)
    {
        bloques_asignados++;
    }
        
    int id_proper = 0;    
    int byte_para_inodo = new_inodo(ruta,startPart);
    
    inodo nuevo = getINodo(ruta,byte_para_inodo);
    nuevo.i_count_bloques_asignados = bloques_asignados;
    nuevo.i_count_inodo = 1;
    nuevo.i_id_proper = id_proper;
    nuevo.i_size_archivo = size_contenido;
    
    setINodo(ruta,byte_para_inodo,nuevo);   
    asignar_dd(ruta,startPart,from_avd_ap_dd,byte_para_inodo,nombre);
   
    agregarContEnBloques(byte_para_inodo,ruta,startPart,contenido,0,0);
    
}

void asignar_dd(char ruta[300],int startPart, int apt_dd, int byte_para_inodo, char nombre[16])
{
    detalleDirectorio ddir = getDetalleDirectorio(ruta,apt_dd);
    int aptr_ind = ddir.dd_ap_detalle_directorio;
    
    if(ddir.dd_archive_1.dd_file_ap_inodo == 0)
    {
        ddir.dd_archive_1.dd_file_ap_inodo = byte_para_inodo;
        strcpy(ddir.dd_archive_1.dd_file_nombre,nombre);
        ddir.dd_archive_1.dd_file_date_creacion=time(0);
        ddir.dd_archive_1.dd_file_date_modificacion=time(0);
    }
    else if(ddir.dd_archive_2.dd_file_ap_inodo == 0)
    {
        ddir.dd_archive_2.dd_file_ap_inodo = byte_para_inodo;
        strcpy(ddir.dd_archive_2.dd_file_nombre,nombre);
        ddir.dd_archive_2.dd_file_date_creacion=time(0);
        ddir.dd_archive_2.dd_file_date_modificacion=time(0);
    }
    else if(ddir.dd_archive_3.dd_file_ap_inodo == 0)
    {
        ddir.dd_archive_3.dd_file_ap_inodo = byte_para_inodo;
        strcpy(ddir.dd_archive_3.dd_file_nombre,nombre);
        ddir.dd_archive_3.dd_file_date_creacion=time(0);
        ddir.dd_archive_3.dd_file_date_modificacion=time(0);
    }
    else if(ddir.dd_archive_4.dd_file_ap_inodo == 0)
    {
        ddir.dd_archive_4.dd_file_ap_inodo = byte_para_inodo;
        strcpy(ddir.dd_archive_4.dd_file_nombre,nombre);
        ddir.dd_archive_4.dd_file_date_creacion=time(0);
        ddir.dd_archive_4.dd_file_date_modificacion=time(0);
    }
    else if(ddir.dd_archive_5.dd_file_ap_inodo == 0)
    {
        ddir.dd_archive_5.dd_file_ap_inodo = byte_para_inodo;
        strcpy(ddir.dd_archive_5.dd_file_nombre,nombre);
        ddir.dd_archive_5.dd_file_date_creacion=time(0);
        ddir.dd_archive_5.dd_file_date_modificacion=time(0);
    }
    else if(aptr_ind == -1)
    {         
        int nuevo_ap_dd = new_detalle_directorio(ruta,startPart);
        ddir.dd_ap_detalle_directorio = nuevo_ap_dd;
        setDetalleDirectorio(ruta,apt_dd,ddir);
        asignar_dd(ruta,startPart,nuevo_ap_dd,byte_para_inodo,nombre);
    }
    else
    {
        asignar_dd(ruta,startPart,aptr_ind,byte_para_inodo,nombre);
    }
        setDetalleDirectorio(ruta,apt_dd,ddir);
}

void leerArchivo(char ruta[300],int startPart, char nombre[16])
{
    printf("    ***********************************************************\n");
    int byte_inodo = buscar_inodo_nombre(ruta,startPart,nombre);
    inodo n_inodo = getINodo(ruta,byte_inodo);
    char cadena[300];
    getContBloques(cadena,n_inodo,ruta);
    printf("    La cadena es:\n");
    printf("    %s\n",cadena);
    printf("    ***********************************************************\n");
}


int buscar_inodo_nombre(char ruta[300],int startParticion,char name[25])
{
    superBoot nuevo_sb = getSuperBoot(ruta,startParticion);
    int num_return=0,cantidad_dd=0;
    cantidad_dd=nuevo_sb.sb_first_free_bit_detalle_directorio;
    int i=0;
    for(i=0;i<cantidad_dd;i++){
        int numInodo = verifica_inodo_existente(ruta,startParticion,i,name); 
        if(numInodo != 0){
            num_return=numInodo;
            cantidad_dd=0;
        }
    }
    return num_return;
}
    
int verifica_inodo_existente(char ruta[300],int startParticion,int num, char name[25])
{
    superBoot nuevo_sb = getSuperBoot(ruta,startParticion);
    int byte_dd = nuevo_sb.sb_ap_detalle_directorio+num*sizeof(detalleDirectorio);
    detalleDirectorio nuevo_dd = getDetalleDirectorio(ruta, byte_dd);
    int k=0;
    int numd=0;
            
       k=strcasecmp(nuevo_dd.dd_archive_1.dd_file_nombre,name);
       if(k==0){
       numd=nuevo_dd.dd_archive_1.dd_file_ap_inodo;
       }
       
       k=strcasecmp(nuevo_dd.dd_archive_2.dd_file_nombre,name);
       if(k==0){
        numd=nuevo_dd.dd_archive_2.dd_file_ap_inodo;
       }
       
       k=strcasecmp(nuevo_dd.dd_archive_3.dd_file_nombre,name);
       if(k==0){
        numd=nuevo_dd.dd_archive_3.dd_file_ap_inodo;
       }
       
       k=strcasecmp(nuevo_dd.dd_archive_4.dd_file_nombre,name);
       if(k==0){
        numd=nuevo_dd.dd_archive_4.dd_file_ap_inodo;
       }
       
       k=strcasecmp(nuevo_dd.dd_archive_5.dd_file_nombre,name);
       if(k==0){
        numd=nuevo_dd.dd_archive_5.dd_file_ap_inodo;
       }
    return numd ;
}

void getContBloques(char retCad[300], inodo p_inodo, char ruta[300])
{
    int n = 0;
    for(n;n<4;n++){
        if(p_inodo.i_array_bloques[n] != 0){
            int apuntador_bloque = p_inodo.i_array_bloques[n];
            bloque n_bloque = getBloque(ruta,apuntador_bloque);
            char contenidoB[25];
            strncpy(contenidoB,n_bloque.dbdata,25);
            strncat(retCad,contenidoB,25);
        }
    }

    int ptr_new_inodo = p_inodo.i_ap_indirecto; 
    if(ptr_new_inodo != 0)
    {
        inodo nuevito = getINodo(ruta,ptr_new_inodo);
        getContBloques(retCad,nuevito,ruta);
    }
}

/*------------------------------------------------------------------------------------*/
/*---------------METODOS Y FUNCIONES PARA DIRECTORIOS Y ARCHIVOS----------------------*/
/*------------------------------------------------------------------------------------*/

void new_directory(char ruta[300],int startPart,char path[300],int p1)
{
    printf("    ***********************************************************\n");
    superBoot sb1 = getSuperBoot(ruta,startPart);
    int byte_first_avd = sb1.sb_ap_arbol_directorio;
    bool es_directorio = true;
    char carpeta_a_crear[16]; 
       
        listaDir *listaDirectorios = crearOrdenDirectorio();
        char *temp1;
        char *rest1 =path;
        while((temp1 = strtok_r(rest1, "/", &rest1)))
        {
            if(strchr(temp1,'.') == 0){
              insertar(temp1,listaDirectorios);
            }
            else
            {
                es_directorio = false;
                printf("    Error, para crear archivos usar mkfile\n");
            }
        }
        
        strncpy(carpeta_a_crear, listaDirectorios->cabeza->nombre, 16);
        int byte_padre = 0;
        if(es_directorio){
            if(p1==1){
                byte_padre = buscar_crear(listaDirectorios,byte_first_avd,ruta,startPart);
            }else{
                byte_padre = buscar_sin_crear(listaDirectorios,byte_first_avd,ruta,startPart,'d');
                 if(byte_padre != 0)
                    {
                        asignar_hijo_avd(ruta,byte_padre,startPart,carpeta_a_crear);
                        printf("        Directorio agregado con exito ヽ(OwO)ﾉ\n");
                    }
                    else 
                    {
                        printf("        Directorio no creado (T﹏T)\n");
                    }
            }
        }
    printf("    ***********************************************************\n");
}

void new_file(char ruta[300],int startPart,char path[300],char contenido[256],int p1)
{
    printf("    ***********************************************************\n");
    superBoot sb1 = getSuperBoot(ruta,startPart);
    int byte_first_avd = sb1.sb_ap_arbol_directorio;
    int byte_para_file = 0;
       
        listaDir *listaDirectorios = crearOrdenDirectorio();
        char *temp1;
        char *rest1 =path;
        char fileName[16];
        while((temp1 = strtok_r(rest1, "/", &rest1)))
        {
            if(strchr(temp1,'.') == 0)
            {
              insertar(temp1,listaDirectorios);
            }
            else
            {
               strncpy(fileName,temp1,16);
            }
        }
        
        if(p1==1){
            byte_para_file = buscar_crear(listaDirectorios,byte_first_avd,ruta,startPart);
        }else{
            byte_para_file = buscar_sin_crear(listaDirectorios,byte_first_avd,ruta,startPart,'f');
        }
        
        if(byte_para_file != 0)
        {
            arbolVirtualDirectorio ultimo_avd = getAVD(ruta,byte_para_file);
            int byte_dd = ultimo_avd.avd_ap_detalle_directorio;
            agregarArchivo(ruta,startPart,contenido,fileName,byte_dd);
            printf("        Archivo agregado con exito ヽ(OwO)ﾉ\n");
        }
        else
        {
            printf("        Archivo no creado (T﹏T)\n");
        };
    printf("    ***********************************************************\n");
}

int buscar_sin_crear(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart, char f_d)
{
    char *dir = getNodo(listaDirectorios);
    int byte_actual = byte_inicio_avd;
    char a_crear[16];
    strncpy(a_crear,listaDirectorios->cabeza->nombre,16);
    int cmp = -1;
        
     while(dir != NULL && cmp !=0)
     {
            int byte_nuevo_nivel = verificar_si_existe_avd(ruta,byte_actual,dir); 
            if(byte_nuevo_nivel==0)
            {
                printf("    No puede agregar el archivo porque no existen todos los directorios \n");
                
                dir = NULL;
                byte_actual = 0;
            }
            else 
            {
                byte_actual = byte_nuevo_nivel;
                dir = getNodo(listaDirectorios); 
                if(f_d =='d')
                {
                    cmp = strcmp(a_crear,dir);
                }
            }
      }
      return byte_actual;
}

int buscar_crear(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart)
{
    char *dir = getNodo(listaDirectorios); 
    int byte_actual = byte_inicio_avd; // coloco como byte inicial el byte de la carpeta raiz    
    
     while(dir != NULL)
     {
            int byte_nuevo_nivel = verificar_si_existe_avd(ruta,byte_actual,dir); 
            if(byte_nuevo_nivel==0)
            {
                printf("El nombre es_________%s\n",dir);
                int n_byte = asignar_hijo_avd(ruta,byte_actual,startPart,dir);
                byte_actual = n_byte;
            }
            else 
            {
                byte_actual = byte_nuevo_nivel; 
            }
                dir = getNodo(listaDirectorios); 
      }
     recorrer_avd(ruta,byte_inicio_avd);
     return byte_actual;
}

int verificar_si_existe_avd(char ruta[300],int byte_inicio_avd,char nombre[16])
{
    int byte_sig_carpeta = 0;
    int cont = 0;
    arbolVirtualDirectorio nuevo_avd = getAVD(ruta,byte_inicio_avd);
    
       while(byte_sig_carpeta == 0 && cont < 6)
        {
            if(nuevo_avd.avd_ap_array_subdirectorios[cont]!=0)
                {
                byte_sig_carpeta = verificar_nombre_igual(ruta,nuevo_avd.avd_ap_array_subdirectorios[cont],nombre);
                if(byte_sig_carpeta!=0)
                    return byte_sig_carpeta;
                }
            cont++;
        }
        
        int ap_indirecto = nuevo_avd.avd_ap_arbol_virtual_directorio; //recojo lo que hay en ap indirecto
        if(ap_indirecto != -1)
            {
                /*si es diferente de -1 llamo al mismo metodo recursivamente con el nuevo byte (el indirecto)*/
                return verificar_si_existe_avd(ruta,ap_indirecto,nombre);
            }
    /*si no encuentra nada byte_sig_carpeta es 0 asi que debe ser creada*/        
    return  byte_sig_carpeta;   
}

int verificar_nombre_igual(char ruta[300],int ap_avdsub, char name[16])
{
    arbolVirtualDirectorio nuevo_avd = getAVD(ruta,ap_avdsub);
    int k=strcmp(nuevo_avd.avd_nombre_directorio,name);
    if(k==0){
        return ap_avdsub;
    }
    return 0;
}

int asignar_hijo_avd(char ruta[300], int byte_padre, int startPart, char nombre[16])
{
    arbolVirtualDirectorio avd_actual = getAVD(ruta,byte_padre);
    int byte_ret = 0;
     if(avd_actual.avd_ap_array_subdirectorios[0]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[0] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else if(avd_actual.avd_ap_array_subdirectorios[1]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[1] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else if(avd_actual.avd_ap_array_subdirectorios[2]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[2] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else if(avd_actual.avd_ap_array_subdirectorios[3]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[3] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else if(avd_actual.avd_ap_array_subdirectorios[4]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[4] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else if(avd_actual.avd_ap_array_subdirectorios[5]==0)
        {
            byte_ret = new_AVD(ruta,startPart,nombre);
            avd_actual.avd_ap_array_subdirectorios[5] = byte_ret;
            setAVD(ruta,byte_padre,avd_actual);
            return byte_ret;
        }
     else
        {
            /*si las carpetas hijo estan llenas, creo un indirecto 
             * y a este indirecto le mando a crear su hijo*/
            int ap_in = avd_actual.avd_ap_arbol_virtual_directorio;
            if(ap_in == -1)
            {
                int nuevo_padre = new_AVD(ruta,startPart,avd_actual.avd_nombre_directorio);
                avd_actual.avd_ap_arbol_virtual_directorio = nuevo_padre;
                setAVD(ruta,byte_padre,avd_actual);
                return asignar_hijo_avd(ruta,nuevo_padre,startPart,nombre);
            }
            else
            {
                return asignar_hijo_avd(ruta,ap_in,startPart,nombre);
            }
        }
}

     
/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA REPORTAR--------------------------*/
/*------------------------------------------------------------------------------------*/

void reporte_avd(char ruta[300], int startPart)
{    
    superBoot sb = getSuperBoot(ruta,startPart);
    int byte_ini = sb.sb_ap_arbol_directorio;
    
    FILE *grafo = fopen("/home/leslie/Escritorio/Reportes/avd.dot","w+");
    fprintf(grafo,"digraph{ \n");
    fprintf(grafo,"rankdir=TB\n");
    fprintf(grafo,"edge[minlen=2];\n");
    fprintf(grafo,"graph [overlap=false outputorder=edgesfirst];\n");
    fprintf(grafo,"node [shape=record,style=filled];\n");
    fprintf(grafo,"splines=\"line\" ;\n");
    fprintf(grafo,"style = filled;\n\n");
    
    recorrido_avd_subgraphs(ruta,byte_ini,grafo);
    recorrido_avd_ptr(ruta,byte_ini,grafo);
    
    fprintf(grafo,"}\n");
    fclose(grafo);
        
    system("dot -Tpng /home/leslie/Escritorio/Reportes/avd.dot -o /home/leslie/Escritorio/Reportes/avd.png");
}

void recorrido_avd_subgraphs(char ruta[300], int byte_raiz, FILE *g)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);
    char *nombre_carpeta = avd.avd_nombre_directorio;
    int ptr0 = avd.avd_ap_array_subdirectorios[0];
    int ptr1 = avd.avd_ap_array_subdirectorios[1];
    int ptr2 = avd.avd_ap_array_subdirectorios[2];
    int ptr3 = avd.avd_ap_array_subdirectorios[3];
    int ptr4 = avd.avd_ap_array_subdirectorios[4];
    int ptr5 = avd.avd_ap_array_subdirectorios[5];
    int ptri = avd.avd_ap_arbol_virtual_directorio;
    int ptrdd = avd.avd_ap_detalle_directorio;
    
    //------- NODO DEL AVD
    fprintf(g,"nodoavd%d [label = \"{{ <fri%d>|<fh%d> %d |} |",byte_raiz,byte_raiz,byte_raiz,byte_raiz);
    fprintf(g,"\\n %s \\n\\n |", nombre_carpeta);
    fprintf(g,"{");
    fprintf(g,"<f%d> P:%d |",ptr0,ptr0);
    fprintf(g,"<f%d> P:%d |",ptr1,ptr1);
    fprintf(g,"<f%d> P:%d |",ptr2,ptr2);
    fprintf(g,"<f%d> P:%d |",ptr3,ptr3);
    fprintf(g,"<f%d> P:%d |",ptr4,ptr4);
    fprintf(g,"<f%d> P:%d |",ptr5,ptr5);
    fprintf(g,"<f%d> DD:%d |",ptrdd,ptrdd);
    fprintf(g,"<f%d> I:%d |",ptri,ptri);
    fprintf(g,"}}");
    fprintf(g," \",fillcolor=\"yellowgreen\"];\n\n");
    
    /*%%%%%%%%%%%%%%%%%%%%%%%% APLICANDO RECURSIVIDAD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
    int i = 0;
    for(i;i<6;i++){
        int avd_byte_hijo = avd.avd_ap_array_subdirectorios[i]; 
        if(avd_byte_hijo != 0)
        {
            recorrido_avd_subgraphs(ruta,avd_byte_hijo,g);            
        }
    }
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        recorrido_avd_subgraphs(ruta,avd_byte_ind,g);
    }
}


void recorrido_avd_ptr(char ruta[300], int byte_raiz, FILE *g)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);

    int i = 0;
    while(i<6)
    {
        int ptr = avd.avd_ap_array_subdirectorios[i];
        if(ptr != 0)
        {
            fprintf(g,"nodoavd%d:f%d ->  nodoavd%d:fh%d ; \n",byte_raiz,ptr,ptr,ptr);
        }
        i++;
    }
    
    fprintf(g,"\n");
    
    i = 0;
    for(i;i<6;i++){
        int avd_byte_hijo = avd.avd_ap_array_subdirectorios[i]; 
        if(avd_byte_hijo != 0)
        {
            recorrido_avd_ptr(ruta,avd_byte_hijo,g);            
        }
    }
    
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        fprintf(g,"{ rank = \"same\"; nodoavd%d ->  nodoavd%d; } \n",byte_raiz,avd_byte_ind);
        recorrido_avd_ptr(ruta,avd_byte_ind,g);
    }
}

void reporte_tree_complete(char ruta[300], int startPart)
{    
    superBoot sb = getSuperBoot(ruta,startPart);
    int byte_ini = sb.sb_ap_arbol_directorio;
    
    FILE *grafo = fopen("/home/leslie/Escritorio/Reportes/tc.dot","w+");
    fprintf(grafo,"digraph{ \n");
    fprintf(grafo,"rankdir=TB\n");
    fprintf(grafo,"edge[minlen=2];\n");
    fprintf(grafo,"graph [overlap=false outputorder=edgesfirst];\n");
    fprintf(grafo,"node [shape=record,style=filled];\n");
    fprintf(grafo,"splines=\"line\" ;\n");
    fprintf(grafo,"style = filled;\n\n");
    
    recorrido_tc_subgraphs(ruta,byte_ini,grafo);
    recorrido_tc_ptr(ruta,byte_ini,grafo);
    
    fprintf(grafo,"}\n");
    fclose(grafo);
        
    system("dot -Tpng /home/leslie/Escritorio/Reportes/tc.dot -o /home/leslie/Escritorio/Reportes/tc.png");

    
}

void recorrido_tc_subgraphs(char ruta[300], int byte_raiz, FILE *g)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);
    char *nombre_carpeta = avd.avd_nombre_directorio;
    int ptr0 = avd.avd_ap_array_subdirectorios[0];
    int ptr1 = avd.avd_ap_array_subdirectorios[1];
    int ptr2 = avd.avd_ap_array_subdirectorios[2];
    int ptr3 = avd.avd_ap_array_subdirectorios[3];
    int ptr4 = avd.avd_ap_array_subdirectorios[4];
    int ptr5 = avd.avd_ap_array_subdirectorios[5];
    int ptri = avd.avd_ap_arbol_virtual_directorio;
    int ptrdd = avd.avd_ap_detalle_directorio;
    
    //------- NODO DEL AVD
    fprintf(g,"nodoavd%d [label = \"{{ <fri%d>|<fh%d> %d |} |",byte_raiz,byte_raiz,byte_raiz,byte_raiz);
    fprintf(g,"\\n %s \\n\\n |", nombre_carpeta);
    fprintf(g,"{");
    fprintf(g,"<f%d> P:%d |",ptr0,ptr0);
    fprintf(g,"<f%d> P:%d |",ptr1,ptr1);
    fprintf(g,"<f%d> P:%d |",ptr2,ptr2);
    fprintf(g,"<f%d> P:%d |",ptr3,ptr3);
    fprintf(g,"<f%d> P:%d |",ptr4,ptr4);
    fprintf(g,"<f%d> P:%d |",ptr5,ptr5);
    fprintf(g,"<f%d> DD:%d |",ptrdd,ptrdd);
    fprintf(g,"<f%d> I:%d |",ptri,ptri);
    fprintf(g,"}}");
    fprintf(g," \",fillcolor=\"yellowgreen\"];\n\n");
    
   //------------ NODOS DEL DETALLE DE DIRECTORIO
    detalleDirectorio dd = getDetalleDirectorio(ruta,ptrdd);
    recorrido_dd(ruta,g,dd,ptrdd);
    
    //--------- NODOS DEL INODO
    recorrido_dd_p_inodos(ruta,g,dd,ptrdd);
    
    //--------- NODOS DE LOS BLOQUES
    
    /*%%%%%%%%%%%%%%%%%%%%%%%% APLICANDO RECURSIVIDAD %%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/
    int i = 0;
    for(i;i<6;i++){
        int avd_byte_hijo = avd.avd_ap_array_subdirectorios[i]; 
        if(avd_byte_hijo != 0)
        {
            recorrido_tc_subgraphs(ruta,avd_byte_hijo,g);            
        }
    }
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        recorrido_tc_subgraphs(ruta,avd_byte_ind,g);
    }
}

void recorrido_dd(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd)
{
    fprintf(g,"nododd%d[label = \"{{ Nombre Archivo | Fecha Creacion | Fecha Moficacion | i nodo } | ",ptrdd);
    
    int l = 0;
    for(l; l<5; l++){
        archivo file_t;
        switch(l){
            case 0:
            {
               file_t = dd.dd_archive_1;
               break; 
            }
            case 1:
            {
               file_t = dd.dd_archive_2;
               break; 
            }
            case 2:
            {
               file_t = dd.dd_archive_3;
               break; 
            }
            case 3:
            {
               file_t = dd.dd_archive_4;
               break; 
            }
            case 4:
            {
               file_t = dd.dd_archive_5;
               break; 
            }
        }
        // apuntador a inodo
        int ptr_in = file_t.dd_file_ap_inodo;
        if(ptr_in != 0)
        {
            // obtengo nombre
            char nombre_archivo[16];
            strncpy(nombre_archivo,file_t.dd_file_nombre,16);
            // obtengo fecha de mod
            time_t d_m = file_t.dd_file_date_modificacion;
            char date_m[20];
            strftime(date_m, 20, "%d-%m-%Y %H:%M", localtime(&d_m));
            // obtengo fecha de cre
            time_t d_c = file_t.dd_file_date_creacion;
            char date_c[20];
            strftime(date_c, 20, "%d-%m-%Y %H:%M", localtime(&d_c));
            // imprimo los datos
            fprintf(g,"{ %s | %s | %s | <pin%d> %d } | ",nombre_archivo,date_m,date_c,ptr_in,ptr_in);
        }
        else
        {
            fprintf(g,"{ Vacio | 0 | 0 | 0 } | ");
        }
    }
    // indirecto
    int ptr_i_dd = dd.dd_ap_detalle_directorio;
    fprintf(g,"<fid%d> %d}",ptr_i_dd,ptr_i_dd);
    fprintf(g," \",fillcolor=\"crimson\"];\n\n");
    
    if(ptr_i_dd != -1)
    {
        detalleDirectorio n_dd = getDetalleDirectorio(ruta,ptr_i_dd);
        recorrido_dd(ruta,g,n_dd,ptr_i_dd);
    }
}

void recorrido_dd_p_inodos(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd)
{
    int l = 0;
    for(l; l<5; l++){
        archivo file_t;
        switch(l){
            case 0:
            {
               file_t = dd.dd_archive_1;
               break; 
            }
            case 1:
            {
               file_t = dd.dd_archive_2;
               break; 
            }
            case 2:
            {
               file_t = dd.dd_archive_3;
               break; 
            }
            case 3:
            {
               file_t = dd.dd_archive_4;
               break; 
            }
            case 4:
            {
               file_t = dd.dd_archive_5;
               break; 
            }
        }
        // apuntador a inodo
        int ptr_in = file_t.dd_file_ap_inodo;
        if(ptr_in != 0)
        {
            inodo n_inodo = getINodo(ruta,ptr_in);
            recorrido_inodos(ruta,g,n_inodo,ptr_in,ptrdd);
        }
    }
    // indirecto
    int ptr_i_dd = dd.dd_ap_detalle_directorio;    
    if(ptr_i_dd != -1)
    {
        detalleDirectorio n_dd = getDetalleDirectorio(ruta,ptr_i_dd);
        recorrido_dd_p_inodos(ruta,g,n_dd,ptr_i_dd);
    }
}

void recorrido_inodos(char ruta[300], FILE *g, inodo i_nodo,int ptrin,int ptrdd)
{
    int no_inodo = i_nodo.i_count_inodo;
    int size_f = i_nodo.i_size_archivo;
    int no_bloq = i_nodo.i_count_bloques_asignados;
    int apt_in = i_nodo.i_ap_indirecto;
    fprintf(g,"nodoin%d[label= \"{{ inodo | %d } |",ptrin,no_inodo);
    fprintf(g,"{ Size | %d } |",size_f);
    fprintf(g,"{ No. Bloq | %d } |",no_bloq);
    int i = 0;
    for(i; i<4; i++)
    {
        int ptr = i_nodo.i_array_bloques[i];
        fprintf(g,"{ Bloque%d | <fin_b%d> %d } |",i,ptr,ptr);
    }
    fprintf(g,"<fii%d> %d",apt_in,apt_in);
    fprintf(g,"}");
    fprintf(g," \",fillcolor=\"yellow\"];\n\n");
    
    fprintf(g,"nododd%d:pin%d -> nodoin%d ;\n",ptrdd,ptrin,ptrin);
    
    i = 0;
    for(i; i<4; i++)
    {
        int ptrb = i_nodo.i_array_bloques[i];
        if(ptrb != 0){
        bloque n_bloque = getBloque(ruta,ptrb);    
        char cad[26];
        strncpy(cad,n_bloque.dbdata,25);
        cad[25]=0;
        fprintf(g,"nodoi_b%d[label = \"{ Bloque%d | %s }\",fillcolor=\"royalblue\"]; \n\n",ptrb,i,cad);
        fprintf(g," nodoin%d:fin_b%d -> nodoi_b%d ;\n",ptrin,ptrb,ptrb);
        }
    }
    
    if(apt_in != -1)
    {
        recorrido_inodos_indirectos(ruta,g,ptrin,apt_in);
    }
    
}

void recorrido_inodos_indirectos(char ruta[300], FILE *g,int ptrin,int ptrin2)
{
    inodo i_nodo = getINodo(ruta,ptrin2);
    int no_inodo = i_nodo.i_count_inodo;
    int size_f = i_nodo.i_size_archivo;
    int no_bloq = i_nodo.i_count_bloques_asignados;
    int apt_in = i_nodo.i_ap_indirecto;
    fprintf(g,"nodoin%d[label= \"{{ inodo | %d } |",ptrin2,no_inodo);
    fprintf(g,"{ Size | %d } |",size_f);
    fprintf(g,"{ No. Bloq | %d } |",no_bloq);
    int i = 0;
    for(i; i<4; i++)
    {
        int ptr = i_nodo.i_array_bloques[i];
        fprintf(g,"{ Bloque%d | <in_b%d> %d } |",i,ptr,ptr);
    }
    fprintf(g,"<fii%d> %d",apt_in,apt_in);
    fprintf(g,"}");
    fprintf(g," \",fillcolor=\"yellow\"];\n\n");
    
    fprintf(g,"{ rank = \"same\"; nodoin%d -> nodoin%d ;}\n",ptrin,ptrin2);
    
        i = 0;
    for(i; i<4; i++)
    {
        int ptrb = i_nodo.i_array_bloques[i];
        if(ptrb != 0){
        bloque n_bloque = getBloque(ruta,ptrb);    
        char cad[26];
        strncpy(cad,n_bloque.dbdata,25);
        cad[25]=0;
        fprintf(g,"i_b%d[label = \"{ Bloque%d | %s }\",fillcolor=\"royalblue\"]; \n\n",ptrb,i,cad);
        fprintf(g," nodoin%d:in_b%d -> i_b%d ;\n",ptrin2,ptrb,ptrb);
        }
    }
    
    if(apt_in != -1)
    {
        inodo n_i_nodo = getINodo(ruta,apt_in);
        recorrido_inodos_indirectos(ruta,g,ptrin2,apt_in);
    }
}

void recorrido_tc_ptr(char ruta[300], int byte_raiz, FILE *g)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);
    
    // detalles de directorio    
    int avd_byte_dd = avd.avd_ap_detalle_directorio;
    fprintf(g," nodoavd%d:f%d ->  nododd%d ; \n",byte_raiz,avd_byte_dd,avd_byte_dd); 
    detalleDirectorio det= getDetalleDirectorio(ruta,avd.avd_ap_detalle_directorio);
    recorrido_dd_ptr(ruta,g,det,avd_byte_dd);

    int i = 0;
    while(i<6)
    {
        int ptr = avd.avd_ap_array_subdirectorios[i];
        if(ptr != 0)
        {
            fprintf(g,"nodoavd%d:f%d ->  nodoavd%d:fh%d ; \n",byte_raiz,ptr,ptr,ptr);
        }
        i++;
    }
    
    fprintf(g,"\n");
    
    i = 0;
    for(i;i<6;i++){
        int avd_byte_hijo = avd.avd_ap_array_subdirectorios[i]; 
        if(avd_byte_hijo != 0)
        {
            recorrido_tc_ptr(ruta,avd_byte_hijo,g);            
        }
    }
    
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        fprintf(g,"{ rank = \"same\"; nodoavd%d ->  nodoavd%d; } \n",byte_raiz,avd_byte_ind);
        recorrido_tc_ptr(ruta,avd_byte_ind,g);
    }
}

void recorrido_dd_ptr(char ruta[300],FILE *g,detalleDirectorio dd,int ptr1)
{
    int ptr_i_dd = dd.dd_ap_detalle_directorio;
    if(ptr_i_dd != -1)
    {
        fprintf(g,"{ rank = \"same\"; nododd%d ->  nododd%d; }\n",ptr1,ptr_i_dd); 
        detalleDirectorio n_dd = getDetalleDirectorio(ruta,ptr_i_dd);
        recorrido_dd_ptr(ruta,g,n_dd,ptr_i_dd);
    }
    
}

void reportar_inodos(char ruta[300], int startPart)
{
    superBoot sb = getSuperBoot(ruta,startPart);
    int byte_inode = sb.sb_ap_tabla_inodo;
    bitmapTablaINodos bm_in = get_bm_tabla_in(ruta,startPart);
    
    FILE *grafo = fopen("/home/leslie/Escritorio/Reportes/inodos.dot","w+");
    fprintf(grafo,"digraph{ \n");
    fprintf(grafo,"rankdir=TB\n");
    fprintf(grafo,"node [shape=record,style=filled];\n");
    fprintf(grafo,"style = filled;\n\n");
    
    int i = 0;
    char bit;
    do
    {
        bit = bm_in.bmp_tabla_inodos[i];
        if(bit == '1')
        {
            int byte_in_actual = byte_inode + i*sizeof(inodo);
            reportar_rec_inodos(ruta,byte_in_actual,grafo);
        }
        i++;
    }
    while(bit == '0' || bit == '1');
    
    fprintf(grafo,"}\n");
    fclose(grafo);
        
    system("dot -Tpng /home/leslie/Escritorio/Reportes/inodos.dot -o /home/leslie/Escritorio/Reportes/inodos.png");
}

void reportar_rec_inodos(char ruta[300], int byte_ini, FILE *g)
{
    inodo i_nodo = getINodo(ruta,byte_ini);
    
    int no_inodo = i_nodo.i_count_inodo;
    int size_f = i_nodo.i_size_archivo;
    int no_bloq = i_nodo.i_count_bloques_asignados;
    int apt_in = i_nodo.i_ap_indirecto;
    fprintf(g,"nodoin%d[label= \"{{ inodo | %d } |",byte_ini,no_inodo);
    fprintf(g,"{ Size | %d } |",size_f);
    fprintf(g,"{ No. Bloq | %d } |",no_bloq);
    int i = 0;
    for(i; i<4; i++)
    {
        int ptr = i_nodo.i_array_bloques[i];
        fprintf(g,"{ Bloque%d | <fin_b%d> %d } |",i,ptr,ptr);
    }
    fprintf(g,"<fii%d> %d",apt_in,apt_in);
    fprintf(g,"}");
    fprintf(g," \",fillcolor=\"yellow\"];\n\n");
}


void reportar_bloques(char ruta[300], int startPart)
{
    superBoot sb = getSuperBoot(ruta,startPart);
    int byte_bloq = sb.sb_ap_bloques;
    bitmapBloques bm_b = get_bm_bloques(ruta,startPart);
    
    FILE *grafo = fopen("/home/leslie/Escritorio/Reportes/bloques.dot","w+");
    fprintf(grafo,"digraph{ \n");
    fprintf(grafo,"rankdir=TB\n");
    fprintf(grafo,"node [shape=record,style=filled];\n");
    fprintf(grafo,"style = filled;\n\n");
    
    int i = 0;
    char bit;
    do
    {
        bit = bm_b.bmp_bloques[i];
        if(bit == '1')
        {
            int byte_in_actual = byte_bloq + i*sizeof(bloque);
            reportar_rec_bloque(ruta,byte_in_actual,grafo);
        }
        i++;
    }
    while(bit == '0' || bit == '1');
    
    fprintf(grafo,"}\n");
    fclose(grafo);
        
    system("dot -Tpng /home/leslie/Escritorio/Reportes/bloques.dot -o /home/leslie/Escritorio/Reportes/bloques.png");
}

void reportar_rec_bloque(char ruta[300], int byte_ini, FILE *g)
{
    bloque bloq = getBloque(ruta,byte_ini);
    char cad[26];
    strncpy(cad,bloq.dbdata,25);
    cad[25]=0;
    fprintf(g,"i_b%d[label = \"{ Bloque | %s }\",fillcolor=\"royalblue\"]; \n\n",byte_ini,cad);
}

void reportar_sb(char ruta[300], int startPart)
{
    superBoot sb = getSuperBoot(ruta,startPart);
    
    FILE *g = fopen("/home/leslie/Escritorio/Reportes/sb.dot","w+");
    fprintf(g,"digraph{ \n");
    fprintf(g,"rankdir=TB\n");
    fprintf(g,"node [shape=record,style=filled];\n");
    fprintf(g,"style = filled;\n\n");
    
    fprintf(g,"nodo[label= \"{{ Nombre | \\t\\t\\t Valor } |");
    fprintf(g,"{ sb_nombre_hd | %s } |",sb.sb_nombre_hd);
    fprintf(g,"{ sb_arbol_virtual_count | %d } |",sb.sb_arbol_virtual_count);
    fprintf(g,"{ sb_detalle_directorio_count | %d } |",sb.sb_detalle_directorio_count);
    fprintf(g,"{ sb_inodos_count | %d } |",sb.sb_inodos_count);
    fprintf(g,"{ sb_bloques_count | %d } |",sb.sb_bloques_count);
    fprintf(g,"{ sb_arbol_virtual_free | %d } |",sb.sb_arbol_virtual_free);
    fprintf(g,"{ sb_detalle_directorio_free | %d } |",sb.sb_detalle_directorio_free);
    fprintf(g,"{ sb_inodos_free | %d } |",sb.sb_inodos_free);
    fprintf(g,"{ sb_bloques_free | %d } |",sb.sb_bloques_free);
    fprintf(g,"{ sb_date_creacion | %d } |",sb.sb_date_creacion);
    fprintf(g,"{ sb_date_ultimo_montaje | %d } |",sb.sb_date_ultimo_montaje);
    fprintf(g,"{ sb_montajes_count | %d } |",sb.sb_montajes_count);
    fprintf(g,"{ sb_ap_bitmap_arbol_directorio| %d } |",sb.sb_ap_bitmap_arbol_directorio);
    fprintf(g,"{ sb_ap_arbol_directorio | %d } |",sb.sb_ap_arbol_directorio);
    fprintf(g,"{ sb_ap_bitmap_detalle_directorio | %d } |",sb.sb_ap_bitmap_detalle_directorio);
    fprintf(g,"{ sb_ap_detalle_directorio  | %d } |",sb.sb_ap_detalle_directorio);
    fprintf(g,"{ sb_ap_bitmap_tabla_inodo | %d } |",sb.sb_ap_bitmap_tabla_inodo);
    fprintf(g,"{ sb_ap_tabla_inodo | %d } |",sb.sb_ap_tabla_inodo);
    fprintf(g,"{ sb_ap_bitmap_bloques| %d } |",sb.sb_ap_bitmap_bloques);
    fprintf(g,"{ sb_ap_log | %d } |",sb.sb_ap_log);
    fprintf(g,"{ sb_size_struct_arbol_directorio | %d } |",sb.sb_size_struct_arbol_directorio);
    fprintf(g,"{ sb_size_struct_detalle_directorio | %d } |",sb.sb_size_struct_detalle_directorio);
    fprintf(g,"{ sb_size_struct_inodo | %d } |",sb.sb_size_struct_inodo);
    fprintf(g,"{ sb_first_free_bit_arbol_directorio | %d } |",sb.sb_first_free_bit_arbol_directorio);
    fprintf(g,"{ sb_first_free_bit_detalle_directorio | %d } |",sb.sb_first_free_bit_detalle_directorio);
    fprintf(g,"{ sb_first_free_bit_tabla_inodo | %d } |",sb.sb_first_free_bit_tabla_inodo);
    fprintf(g,"{ sb_first_free_bit_bloque | %d } |",sb.sb_first_free_bit_bloque);
    fprintf(g,"{ sb_magic_num | %d }",sb.sb_magic_num);
    fprintf(g," }\",fillcolor=\"blueviolet\"];\n");
    
    
    fprintf(g,"}\n");
    fclose(g);
        
    system("dot -Tpng /home/leslie/Escritorio/Reportes/sb.dot -o /home/leslie/Escritorio/Reportes/sb.png");
}

void recorrer_avd(char ruta[300], int byte_raiz)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);
    printf("    **************Nodo: %s -> |%d|*************\n",avd.avd_nombre_directorio,byte_raiz);
    printf("    apt detalle dir: %d\n",avd.avd_ap_detalle_directorio);
    printf("    fecha creación:\n");
    printf("    id user: %d\n\n",avd.avd_proper);
    int i = 0;
    for(i;i<6;i++){
        int avd_byte_hijo = avd.avd_ap_array_subdirectorios[i]; 
        if(avd_byte_hijo != 0)
        {
            recorrer_avd(ruta,avd_byte_hijo);            
        }
    }
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        recorrer_avd(ruta,avd_byte_ind);
    }
}
   

   
void imprimir_bitmap_dd(char ruta[300],int startPart)
{
    bitmapDetalleDirectorio bmdd = get_bm_dd(ruta,startPart); 
    FILE *archivoptr;
    archivoptr = fopen("/home/leslie/Escritorio/Reportes/bitmapDD.txt","w+");
    int i = 0;
    for(i;i<10000;i++)
    {
        if(i%20 == 0 && i!= 0)
        {
            fprintf(archivoptr,"\n");
        }
        char bit = bmdd.bmp_detalle_directorio[i];
        if(bit == '0' || bit == '1')
        {
            fprintf(archivoptr,"%c,",bit);
        }
    }
    fclose(archivoptr);
}

void imprimir_bitmap_avd(char ruta[300],int startPart)
{
    bitmapArbolDirectorio bmavd = get_bm_avd(ruta,startPart); 
    FILE *archivoptr;
    archivoptr = fopen("/home/leslie/Escritorio/Reportes/bitmapAVD.txt","w+");
    int i = 0;
    for(i;i<10000;i++)
    {
        if(i%20 == 0 && i!= 0)
        {
            fprintf(archivoptr,"\n");
        }
        char bit = bmavd.bmp_arbol_directorio[i];
        if(bit == '0' || bit == '1')
        {
            fprintf(archivoptr,"%c,",bit);
        }
    }
    fclose(archivoptr);
}

void imprimir_bitmap_inodos(char ruta[300],int startPart)
{
    bitmapTablaINodos bminodos = get_bm_tabla_in(ruta,startPart); 
    FILE *archivoptr;
    archivoptr = fopen("/home/leslie/Escritorio/Reportes/bitmapINODOS.txt","w+");
    int i = 0;
    for(i;i<10000;i++)
    {
        if(i%20 == 0 && i!= 0)
        {
            fprintf(archivoptr,"\n");
        }
        char bit = bminodos.bmp_tabla_inodos[i];
        if(bit == '0' || bit == '1')
        {
            fprintf(archivoptr,"%c,",bit);
        }
    }
    fclose(archivoptr);
}

void imprimir_bitmap_bloques(char ruta[300],int startPart)
{
    bitmapBloques bmbloq = get_bm_bloques(ruta,startPart); 
    FILE *archivoptr;
    archivoptr = fopen("/home/leslie/Escritorio/Reportes/bitmapBLOQ.txt","w+");
    int i = 0;
    for(i;i<10000;i++)
    {
        if(i%20 == 0 && i!= 0)
        {
            fprintf(archivoptr,"\n");
        }
        char bit = bmbloq.bmp_bloques[i];
        if(bit == '0' || bit == '1')
        {
            fprintf(archivoptr,"%c,",bit);
        }
    }
    fclose(archivoptr);
}

void reportar_file(char ruta[300], int startPart, char rutaSA[300])
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    int byte_first_avd = sb1.sb_ap_arbol_directorio;
       
    listaDir *listaDirectorios = crearOrdenDirectorio();
    char *temp1;
    char *rest1 =rutaSA;
    char fileName[16];
    while((temp1 = strtok_r(rest1, "/", &rest1)))
    {
        if(strchr(temp1,'.') == 0)
        {
            insertar(temp1,listaDirectorios);
        }
        else
        {
            strncpy(fileName,temp1,16);
        }
    }
    
    FILE *g = fopen("/home/leslie/Escritorio/Reportes/file.dot","w+");
    fprintf(g,"digraph{ \n");
    fprintf(g,"rankdir=TB\n");
    fprintf(g,"node [shape=record,style=filled];\n");
    fprintf(g,"style = filled;\n\n");
    
    int ultimo_byte = graficar_por_nivel(listaDirectorios,byte_first_avd,ruta,startPart,g);
    arbolVirtualDirectorio last_avd = getAVD(ruta,ultimo_byte);
    int byte_dd = last_avd.avd_ap_detalle_directorio;
    detalleDirectorio dd = getDetalleDirectorio(ruta,byte_dd);
    fprintf(g,"{ rank = \"same\"; nodoavd%d ->  nododd%d; } \n",ultimo_byte,byte_dd);
    graficar_archivo_en_dd(ruta,g,dd,byte_dd,fileName);
    fprintf(g,"}\n");
    fclose(g);
    
    system("dot -Tpng /home/leslie/Escritorio/Reportes/file.dot -o /home/leslie/Escritorio/Reportes/file.png");
}

void graficar_archivo_en_dd(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd,char nombre[16])
{
    fprintf(g,"nododd%d[label = \"{{ Nombre Archivo | Fecha Creacion | Fecha Moficacion | i nodo } | ",ptrdd);
    int ptr_inodo = 0;
    int l = 0;
    for(l; l<5; l++){
        archivo file_t;
        switch(l){
            case 0:
            {
               file_t = dd.dd_archive_1;
               break; 
            }
            case 1:
            {
               file_t = dd.dd_archive_2;
               break; 
            }
            case 2:
            {
               file_t = dd.dd_archive_3;
               break; 
            }
            case 3:
            {
               file_t = dd.dd_archive_4;
               break; 
            }
            case 4:
            {
               file_t = dd.dd_archive_5;
               break; 
            }
        }
        // apuntador a inodo
        int ptr_in = file_t.dd_file_ap_inodo;
        if(ptr_in != 0)
        {
            // obtengo nombre
            char nombre_archivo[16];
            strncpy(nombre_archivo,file_t.dd_file_nombre,16);
            int nom_cmp = strcmp(nombre_archivo,nombre_archivo);
            if(nom_cmp == 0)
            {
                ptr_inodo = ptr_in;
            }
            // obtengo fecha de mod
            time_t d_m = file_t.dd_file_date_modificacion;
            char date_m[20];
            strftime(date_m, 20, "%d-%m-%Y %H:%M", localtime(&d_m));
            // obtengo fecha de cre
            time_t d_c = file_t.dd_file_date_creacion;
            char date_c[20];
            strftime(date_c, 20, "%d-%m-%Y %H:%M", localtime(&d_c));
            // imprimo los datos
            fprintf(g,"{ %s | %s | %s | <pin%d> %d } | ",nombre_archivo,date_m,date_c,ptr_in,ptr_in);
        }
        else
        {
            fprintf(g,"{ Vacio | 0 | 0 | 0 } | ");
        }
    }
    // indirecto
    int ptr_i_dd = dd.dd_ap_detalle_directorio;
    fprintf(g,"<fid%d> %d}",ptr_i_dd,ptr_i_dd);
    fprintf(g," \",fillcolor=\"crimson\"];\n\n");
    
    if(ptr_inodo != 0)
    {
        inodo i_nod = getINodo(ruta,ptr_inodo);
        recorrido_inodos(ruta,g,i_nod,ptr_inodo,ptrdd);
    }
    
    if(ptr_i_dd != -1)
    {
        detalleDirectorio n_dd = getDetalleDirectorio(ruta,ptr_i_dd);
        graficar_archivo_en_dd(ruta,g,n_dd,ptr_i_dd,nombre);
    }
}

int graficar_por_nivel(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart, FILE *g)
{
    char *dir = getNodo(listaDirectorios); 
    int byte_actual = byte_inicio_avd; // coloco como byte inicial el byte de la carpeta raiz    
    
    arbolVirtualDirectorio avd = getAVD(ruta,byte_actual);
    char *nombre_carpeta = avd.avd_nombre_directorio;
    int ptr0 = avd.avd_ap_array_subdirectorios[0];
    int ptr1 = avd.avd_ap_array_subdirectorios[1];
    int ptr2 = avd.avd_ap_array_subdirectorios[2];
    int ptr3 = avd.avd_ap_array_subdirectorios[3];
    int ptr4 = avd.avd_ap_array_subdirectorios[4];
    int ptr5 = avd.avd_ap_array_subdirectorios[5];
    int ptri = avd.avd_ap_arbol_virtual_directorio;
    int ptrdd = avd.avd_ap_detalle_directorio;
    //------- NODO DEL AVD
    fprintf(g,"nodoavd%d [label = \"{{ <fri%d>|<fh%d> %d |} |",byte_actual,byte_actual,byte_actual,byte_actual);
    fprintf(g,"\\n %s \\n\\n |", nombre_carpeta);
    fprintf(g,"{");
    fprintf(g,"<f%d> P:%d |",ptr0,ptr0);
    fprintf(g,"<f%d> P:%d |",ptr1,ptr1);
    fprintf(g,"<f%d> P:%d |",ptr2,ptr2);
    fprintf(g,"<f%d> P:%d |",ptr3,ptr3);
    fprintf(g,"<f%d> P:%d |",ptr4,ptr4);
    fprintf(g,"<f%d> P:%d |",ptr5,ptr5);
    fprintf(g,"<f%d> DD:%d |",ptrdd,ptrdd);
    fprintf(g,"<f%d> I:%d |",ptri,ptri);
    fprintf(g,"}}");
    fprintf(g," \",fillcolor=\"yellowgreen\"];\n\n");
    
    while(dir != NULL)
    {
        int byte_nuevo_nivel = verificar_si_existe_avd(ruta,byte_actual,dir); 
        if(byte_nuevo_nivel==0)
        {
            printf("    No existe la carpeta %s \n",dir);
            dir = NULL;
        }
        else 
        {
            arbolVirtualDirectorio avd = getAVD(ruta,byte_nuevo_nivel);
            char *nombre_carpeta = avd.avd_nombre_directorio;
            int ptr0 = avd.avd_ap_array_subdirectorios[0];
            int ptr1 = avd.avd_ap_array_subdirectorios[1];
            int ptr2 = avd.avd_ap_array_subdirectorios[2];
            int ptr3 = avd.avd_ap_array_subdirectorios[3];
            int ptr4 = avd.avd_ap_array_subdirectorios[4];
            int ptr5 = avd.avd_ap_array_subdirectorios[5];
            int ptri = avd.avd_ap_arbol_virtual_directorio;
            int ptrdd = avd.avd_ap_detalle_directorio;
            //------- NODO DEL AVD
            fprintf(g,"nodoavd%d [label = \"{{ <fri%d>|<fh%d> %d |} |",byte_nuevo_nivel,byte_nuevo_nivel,byte_nuevo_nivel,byte_nuevo_nivel);
            fprintf(g,"\\n %s \\n\\n |", nombre_carpeta);
            fprintf(g,"{");
            fprintf(g,"<f%d> P:%d |",ptr0,ptr0);
            fprintf(g,"<f%d> P:%d |",ptr1,ptr1);
            fprintf(g,"<f%d> P:%d |",ptr2,ptr2);
            fprintf(g,"<f%d> P:%d |",ptr3,ptr3);
            fprintf(g,"<f%d> P:%d |",ptr4,ptr4);
            fprintf(g,"<f%d> P:%d |",ptr5,ptr5);
            fprintf(g,"<f%d> DD:%d |",ptrdd,ptrdd);
            fprintf(g,"<f%d> I:%d |",ptri,ptri);
            fprintf(g,"}}");
            fprintf(g," \",fillcolor=\"yellowgreen\"];\n\n");
                
            fprintf(g,"nodoavd%d ->  nodoavd%d; \n",byte_actual,byte_nuevo_nivel);
            
            byte_actual = byte_nuevo_nivel; 
            dir = getNodo(listaDirectorios); 
        }
    }
     return byte_actual;
}

void reportar_directorio(char ruta[300], int startPart, char rutaSA[300])
{
    superBoot sb1 = getSuperBoot(ruta,startPart);
    int byte_first_avd = sb1.sb_ap_arbol_directorio;
       
    listaDir *listaDirectorios = crearOrdenDirectorio();
    char *temp1;
    char *rest1 =rutaSA;
    char fileName[16];
    while((temp1 = strtok_r(rest1, "/", &rest1)))
    {
        if(strchr(temp1,'.') == 0)
        {
            insertar(temp1,listaDirectorios);
        }
        else
        {
            strncpy(fileName,temp1,16);
        }
    }
    
    int byte_directory = buscar_sin_crear(listaDirectorios,byte_first_avd,ruta,startPart,'f');
    
    FILE *g = fopen("/home/leslie/Escritorio/Reportes/directorio.dot","w+");
    fprintf(g,"digraph{ \n");
    fprintf(g,"rankdir=TB\n");
    fprintf(g,"node [shape=record,style=filled];\n");
    fprintf(g,"style = filled;\n\n");


    recorrido_avd_ind(ruta,byte_directory,g);
   
   
    fprintf(g,"}\n");
    fclose(g);
    
    system("dot -Tpng /home/leslie/Escritorio/Reportes/directorio.dot -o /home/leslie/Escritorio/Reportes/directorio.png");
}

void recorrido_avd_ind(char ruta[300], int byte_raiz, FILE *g)
{
    arbolVirtualDirectorio avd = getAVD(ruta,byte_raiz);
    char *nombre_carpeta = avd.avd_nombre_directorio;
    int ptr0 = avd.avd_ap_array_subdirectorios[0];
    int ptr1 = avd.avd_ap_array_subdirectorios[1];
    int ptr2 = avd.avd_ap_array_subdirectorios[2];
    int ptr3 = avd.avd_ap_array_subdirectorios[3];
    int ptr4 = avd.avd_ap_array_subdirectorios[4];
    int ptr5 = avd.avd_ap_array_subdirectorios[5];
    int ptri = avd.avd_ap_arbol_virtual_directorio;
    int ptrdd = avd.avd_ap_detalle_directorio;
    
    //------- NODO DEL AVD
    fprintf(g,"nodoavd%d [label = \"{{ <fri%d>|<fh%d> %d |} |",byte_raiz,byte_raiz,byte_raiz,byte_raiz);
    fprintf(g,"\\n %s \\n\\n |", nombre_carpeta);
    fprintf(g,"{");
    fprintf(g,"<f%d> P:%d |",ptr0,ptr0);
    fprintf(g,"<f%d> P:%d |",ptr1,ptr1);
    fprintf(g,"<f%d> P:%d |",ptr2,ptr2);
    fprintf(g,"<f%d> P:%d |",ptr3,ptr3);
    fprintf(g,"<f%d> P:%d |",ptr4,ptr4);
    fprintf(g,"<f%d> P:%d |",ptr5,ptr5);
    fprintf(g,"<f%d> DD:%d |",ptrdd,ptrdd);
    fprintf(g,"<f%d> I:%d |",ptri,ptri);
    fprintf(g,"}}");
    fprintf(g," \",fillcolor=\"yellowgreen\"];\n\n");
    
    int avd_byte_ind = avd.avd_ap_arbol_virtual_directorio;
    if(avd_byte_ind != -1)
    {
        recorrido_avd_ind(ruta,avd_byte_ind,g);
    }
}
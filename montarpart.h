#ifndef MONTARPART_H
#define MONTARPART_H

#include <strings.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct NodoLD{
    int in;
    char path[300];
    char name[300];
    char id[300];
    char nameDisk[300];
} NodoLD;


struct listaD{
    NodoLD valor;
    struct listaD *anteriorD;
    struct listaD *siguienteD;
}*enlace1, *enlace2, *enlace3, *enlace4, *inicio,*temp;
//funciones a utilizar

FILE *grafo;
void insertarAdelanteLD(NodoLD dato);
void printListMountedP();
int buscarD(char name[100]);
int ParaElim(char name[100]);
int NumR(char name[100]);
void eliminarD(int x);
NodoLD getMountedNode(char name[300]);
#endif // MONTARPART_H

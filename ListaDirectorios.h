#ifndef LISTADIRECTORIOS_H
#define LISTADIRECTORIOS_H


#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct nodoDir nodoDir;
typedef struct listaDir listaDir;

struct nodoDir{
    char *nombre;
    struct nodoDir *sig;
};

struct listaDir{
     nodoDir *cabeza;
};

listaDir *crearOrdenDirectorio();
void insertar(char *nombre, listaDir *lista);
char *getNodo(listaDir *lista);

#endif 
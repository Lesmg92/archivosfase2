#ifndef VALIDACIONES_H
#define VALIDACIONES_H
#include <strings.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct VerFer{
    char path[300];
    char name;
} VerFer;


struct VerD{
    VerFer valor;
    struct VerD *anteriorD1;
    struct VerD *siguienteD1;
}*enlace1D, *enlace2D, *enlace3D, *enlace4D, *inicioD,*tempD;
//funciones a utilizar

FILE *grafo;
void VerificarDiscoPart(VerFer dato);
int buscarD12(char name[100]);
int tamanoLista();
char LetraRetrono(char name[100]);
void eliminarDVerificarDiscoPart(int u);
void imprimirNodoDVerificarDiscoPart();
char ParaMontarE(char name[100]);
#endif // VALIDACIONES_H

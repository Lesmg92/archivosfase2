#include "Analizador.h"
#include "GestorDiscos.h"
/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/

void analyzeFile(char path[256]){
    FILE *fptr;
    fptr = fopen(path, "r");
    char linea[256];

    while(!feof(fptr)){
        fgets(linea,256,fptr);
        //printf("\nLINEA: --- %s\n", linea);
        if(!(startsWith("\n",linea)))
        {
            orden(linea);
        }
    }

    fclose(fptr); 
}

bool startsWith(const char *pre, const char *str){
    int n = strncasecmp(pre, str, strlen(pre)) == 0;
    return n;
}

int buscarEspacios(char *token,char s){
        if (!token || s=='\0')
        return 0;
    for (;*token; token++)
        if (*token == s)
            return 1;
    return 0;
}

void quitarEspacios(char tmp[256], char cadena[256]) {
    while((tmp=strchr(cadena,32))!=NULL || (tmp=strchr(cadena,10))!=NULL)
        sprintf(tmp,"%s",tmp+1);
}

void quitarUltimoEspacio(char str[256]) {
    int ultimo = strlen(str)-1;
    char aux = str[ultimo];
    if(aux == 32 || aux == '\n'){
        str++[ultimo] = '\0';
        }
   }

int verificarArchivoE(char path[300]) {
    if(access(path, F_OK ) != -1 ) {
     return 1;
    }
    return 0;
}

void crearDirectorio(char path[300]) {
    char *dir = strdup(path);
    size_t tamDir = strlen(dir);
    size_t i;
    char pathActual[tamDir + 1];
    memset(pathActual, 0, sizeof pathActual);
    pathActual[0] = '/'; 
    int dirExistente = 1;
    for(i = 1; i < tamDir; i++){
        if(path[i] == '/'){
            if(dirExistente == 1) {
                int verificarPath = verificarArchivoE(pathActual);
                if(verificarPath == 0){
                    int status = mkdir(pathActual,0777);
                    if(status == -1) {
                        printf("Error. No se pudo crear el directorio.\n");
                        break;
                    }
                    dirExistente = 0; 
                }
            }
            else 
            {
                int status = mkdir(pathActual,0777);
                if(status == -1) {
                    printf("Error. No se pudo crear el directorio.\n");
                    break;
                }
            }
        }
        pathActual[i] = path[i];
        pathActual[i+1] = '\0';
    }


}

/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/

bool nameDskOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z') || (s[c] >= '0' && s[c] <= '9') || s[c] == '_' || s[c] == '.'|| s[c]==32) {
      } else {
      printf("\t*** Error nombre mal Disco\n");  
      return false;
      }
      c++;
   }
   if(strstr(s, ".dsk") == NULL) {
      return false;
    }
    return true;
}

bool nameOk(char s[]){
     int c = 0;
   while (s[c] != '\0') {
      if ((s[c] >= 'a' && s[c] <= 'z') || (s[c] >= 'A' && s[c] <= 'Z')  || (s[c] >= '0' && s[c] <= '9') || s[c] == '_') {
      } else {
      return false;
      }
      c++;
   }
    return true;
}

int pathOk(char *str){
    if(strchr(str, 32) != NULL){ 
      if(strchr(str, 34) != NULL){
          if(str[0] == 34 || str[strlen(str)-1] == 34){
              return 0; // 0 = ok -> comillas y espacios dentro de ellas
            }else{
              return 1; // 1 = error comillas fuera de lugar
            }
      } else {
        return 2; // 2 = error contiene espacios y no comillas
      }
    } else if(strchr(str, 34) != NULL){
        return 0; // 0 = ok -> no contiene espacios ni comillas
    }
    return 0; // 3 = error no contiene espacios pero si comillas
}

int limpiarPath(char pathRet[256], char atributo[256]){
        quitarUltimoEspacio(atributo);
        int valPath = pathOk(atributo);
        if(valPath == 0){
            if(startsWith("\"",atributo))
            {
                char *auxp;
                auxp = strtok(atributo,"\"");
                strcpy(pathRet,auxp);
            } else {
                strcpy(pathRet, atributo);
            }
            return 0;
        } 
        else if(valPath == 2)
            printf("Error en el path: contiene espacios y no comillas\n");
        else if(valPath == 1)
            printf("Error en el path: contiene comillas fuera de lugar\n");
        return 1;            
    }

bool unitOk(char car){
    if(car == 'b' || car == 'k' || car == 'm'){
        return true;
    }
    return false;
}

bool typeOk(char car){
    if(car == 'p' || car == 'e' || car == 'l'){
        return true;
    }
    return false;

}

char fitOk(char str[4]){
       if(strcasecmp(str, "bf") == 0)
           return 'b';
       else if(strcasecmp(str, "ff") == 0)
           return 'f';
       else if(strcasecmp(str, "wf") == 0)
           return 'w';
       else
           return 'e';
}

bool deleteOk(const char *str){
       if(strcasecmp(str, "fast") == 0 || strcasecmp(str, "full") == 0 ) {
      return true;
    }
    return false;
}

bool idMountOk(char *str){
        int tam = strlen(str);
        if(tam == 4){
           if(str[0]=='v'){
               if(str[1] == 'd'){
                   if(str[2]>96 && str[2]<123){
                        if(str[3]>47 && str[3]<58){
                            return true;
                        }
                    } else {return false;}
                } else {return false;}
            } else {return false;}
        }
        return false;
    }
    
/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/

void orden(char str[256]){
    
    const char aptr[2] = "->";
    const char s[2] = "%&";
    const char space[2] = " \n";
    
    char *token = "~";
    char *end_token;
    if(!(startsWith("\n",str))){
    token = strtok_r(str, space,&end_token);
    }
/*------------------------------------------------------------------------------------*/
/*------------------------------------COMENTARIO--------------------------------------*/
/*------------------------------------------------------------------------------------*/
    if(startsWith("#", token)){
        printf("Comentario: %s %s \n",token,end_token);
/*------------------------------------------------------------------------------------*/
/*--------------------------------------MKDISK----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkdisk",token)){   
        int mk_size = 0;
        char mk_name[256];
        char mk_path[256];
        char mk_unit = 'm';

        bool bmk_size = false;
        bool bmk_path = false;
        bool bmk_name = false;
        
        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("size",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                mk_size = atoi(atributo);
                int divMod = mk_size % 8;
                if(divMod == 0 && mk_size>0){
                    bmk_size = true;
                }else{
                    printf("**Error el numero en mkdisk no es multiplo de 8 o es negativo\n");
                }
            }else if(startsWith("path",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                int valretpath = limpiarPath(mk_path,atributo);
                if(valretpath == 0){
                    bmk_path =  true;
                }
            }else if(startsWith("name",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                strcpy(mk_name, atributo);
                quitarUltimoEspacio(mk_name);
                if(nameDskOk(mk_name)){
                    bmk_name = true;
                }
            }else if(startsWith("unit",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                mk_unit = atributo[0];
                if(!unitOk(mk_unit)){
                    // error en unit
                    mk_unit = 'k';
                }
            }else {
                bmk_name = false;
            }
            token = strtok_r(NULL,s,&end_token);
        } 
        
        if(bmk_name && bmk_path && bmk_size){
            printf("\nLos parametros MKDISK son: \nsize:%d \npath:%s \nname:[%s] \nunit:%c \n", mk_size, mk_path, mk_name, mk_unit);
            strcat(mk_path,"/"); 
            crearDirectorio(mk_path);
            strcat(mk_path,mk_name);
            crearDisco(mk_path,mk_size,mk_unit);
        }else{
            printf("\nPARAMETROS MAL MKDISK\n");
        }
/*------------------------------------------------------------------------------------*/
/*-------------------------------------RMIDISK----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("rmdisk",token)){
        char rm_path[256];
        
        bool itsOk = false;
        
        token = strtok_r(NULL,s,&end_token);

        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("path", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                 int valretpath = limpiarPath(rm_path,atributo);
                 if(valretpath == 0){
                     itsOk =  true;
                }
            }else{
                    printf("\nPARAMETROS MAL RMDISK\n");
                    itsOk = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
            
        if(itsOk){
            eliminarDisco(rm_path);
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------FDISK----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("fdisk",token)){ 
        int f_size = 0;
        char f_unit = 'k';
        char f_path[256];
        char f_type = 'p';
        char f_fit = 'w';
        char f_delete[256]; // verificar junto a name y path
        char f_name[256];
        int f_add = 0;  // valor unit

        bool bf_size = false;
        bool bf_path = false;
        bool bf_name = false;
        bool bf_useDelete = false;
        bool bf_useAdd = false;

        token = strtok_r(NULL,s,&end_token);
    
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
                        
            if(startsWith("size",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                f_size = atoi(atributo);
                int divMod = f_size % 8;
                if(divMod == 0 && f_size>0){
                    bf_size = true;
                }else{
                    printf("**Error el numero en fdisk no es multiplo de 8 o es negativo\n");
                }
            }else if(startsWith("unit",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                f_unit = atributo[0];
                if(!unitOk(f_unit)){
                            // error en unit
                f_unit = 'k';
                }
            }else if(startsWith("path",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                int valretpath = limpiarPath(f_path,atributo);
                if(valretpath == 0){
                    bf_path =  true;
                }
            }else if(startsWith("type",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                f_type = atributo[0];
                if(!typeOk(f_type)){
                    // error en type
                    f_type = 'p';
                }
            }else if(startsWith("fit",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                char auxfit[4];
                strcpy(auxfit,atributo);
                f_fit = fitOk(auxfit);
                if(f_fit == 'e'){
                    f_fit = 'w';
                }
            }else if(startsWith("delete",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarEspacios(f_delete,atributo);
                if(!deleteOk(f_delete)){
                    // error en delete
                }
                bf_useDelete = true;
            }else if(startsWith("name",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarEspacios(f_name,atributo);
                strcpy(f_name,atributo);
                bf_name = true;
            }else if(startsWith("add",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                f_add = atoi(atributo);
                bf_useAdd = true;
            } else {
                bf_name = false;
                bf_path = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
        //// ------> verificar parametros completos para el uso de fdisk
        if(bf_useDelete){
            if(bf_name && bf_path){
                printf("Los parametros FDISK(DELETE) son: \nname:%s \npath:%s \n", f_name, f_path);
                eliminarParticion(f_path,f_name);
            }else{
                printf("PARAMETROS MAL FDISK DELETE\n");
            }
        } else if(bf_useAdd){
            if(bf_name){
                if(f_unit=='m' || f_unit=='M') 
                    f_add=f_add*mb;
                else if(f_unit=='k' || f_unit=='K')
                    f_add=f_add*kb;
                printf("Los parametros FDISK(ADD) son: \nname:%s \nunit:%c \n", f_name, f_unit);
                modificarParticion(f_path,f_name,f_add);
            }else{
                printf("PARAMETROS MAL FDISK ADD\n");
            }
        } else {
            if(bf_name && bf_path && bf_size){
                printf("Los parametros FDISK son: \nsize:%d \npath:%s \nname:%s \nunit:%c \n", f_size, f_path, f_name, f_unit);
                if(f_unit=='m' || f_unit=='M') 
                    f_size=f_size*mb;
                    else if(f_unit=='k' || f_unit=='K')
                    f_size=f_size*kb;        
                    crearParticion(f_path, f_name, f_type,f_fit,f_size);
            }else{
                printf("PARAMETROS MAL FDISK\n");
            }
        }
/*------------------------------------------------------------------------------------*/
/*--------------------------------------MOUNT-----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mount",token)){
        char m_path[256];
        char m_name[50];

        bool bm_name = false;
        bool bm_path = false;

        token = strtok_r(NULL,s,&end_token);
        
        if(token != NULL)
        {
            while(token != NULL)
            {
                char *atributo;
                char *end_atributo;
                          
                if(startsWith("name",token)){
                    atributo = strtok_r(token,aptr,&end_atributo);
                    atributo = strtok_r(NULL,aptr,&end_atributo);
                    strcpy(m_name,atributo);
                    quitarUltimoEspacio(m_name);
                    if(nameOk(m_name)){
                        bm_name = true;
                    }
                }else if(startsWith("path",token)){
                    atributo = strtok_r(token,aptr,&end_atributo);
                    atributo = strtok_r(NULL,aptr,&end_atributo);
                    int valretpath = limpiarPath(m_path,atributo);
                    if(valretpath == 0){
                        bm_path =  true;
                    }
                }else {
                    bm_name = false;
                    bm_path = false;
                }
                token = strtok_r(NULL,s,&end_token);
            }
        }
        
        if(bm_name && bm_path){
            printf("\nLos parametros MOUNT son: \npath:%s \nname:%s \n", m_path, m_name);
            mountP(m_path, m_name);
            // imprimirNodoDVerificarDiscoPart();
            // printListMountedP();
        }else{
            printf("    ***************************************\n");
            printf("    Valores Montados:\n");
            printListMountedP();
            printf("    ***************************************\n");
        }
/*------------------------------------------------------------------------------------*/
/*-------------------------------------UNMOUNT----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("unmount",token)){
        char umo_id[256];
        
        bool itsOk = false;
        
        token = strtok_r(NULL,s,&end_token);

        while( token != NULL ){
            char *atributo;
            char *end_atributo;
             
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(umo_id, atributo);
                    itsOk = true;
                } else {
                    itsOk = false;
                    printf("Unmount error en el id\n");
                }
            }else {
                itsOk = false;
            }
            token = strtok_r(NULL,s,&end_token);
         }
         
         if(itsOk){
                unmountP(umo_id);
        }
/*------------------------------------------------------------------------------------*/
/*--------------------------------------MKFS------------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkfs",token)){
        char mkfs_type[6];
        char mkfs_id[5];
        int mkfs_add;
        char mkfs_unit;
        
        bool bmkfs_id = false;
        bool bmkfs_add = false;
        
        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                strcpy(mkfs_id, atributo);
                quitarUltimoEspacio(mkfs_id);
                bmkfs_id = true;
            }else if(startsWith("type", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarEspacios(mkfs_type,atributo);
                if(!deleteOk(mkfs_type)){
                   // error en delete
                }
            }else if(startsWith("add", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                mkfs_add = atoi(atributo);
                bmkfs_add = true;
            }else if(startsWith("unit", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                mkfs_unit = atributo[0];
                if(!unitOk(mkfs_unit)){
                   // error en unit
                   mkfs_unit = 'k';
                }
            }
            token = strtok_r(NULL,s,&end_token);
        }               
             
        if(bmkfs_id){
            if(bmkfs_add)
            {
                //  modificarFormatoPart(mkfs_id, mkfs_add, mkfs_unit);       
            }else{
                NodoLD auxNod = getMountedNode(mkfs_id);
                partition auxPart = getPart(mkfs_id);
                formatPart(auxNod.path, auxPart.part_name,auxPart.part_size,auxPart.part_start);   
            }
        }else{
            printf("En mkfs falta id o es incorrecto\n");
        }
/*------------------------------------------------------------------------------------*/
/*-------------------------------------MKFILE-----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkfile",token)){
        char mkf_id[50];
        char mkf_path[256];
        int mkf_p = 0;
        int mkf_size = 0;
        char mkf_cont[256];
        
        bool bmkf_id = false;
        bool bmkf_path = false;
        bool bmkf_cont = false;
                
        token = strtok_r(NULL,s,&end_token);
    
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(mkf_id,atributo);
                    bmkf_id = true;
                }
            }else if(startsWith("path",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkf_path,atributo);
                bmkf_path = true;
            }else if(startsWith("p",token)){
                mkf_p = 1;
            }else if(startsWith("size",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                mkf_size = atoi(atributo);
            }else if(startsWith("cont",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkf_cont,atributo);
                bmkf_cont = true;
            }else{
                printf("Error, atributos extra en rmusr\n");
                bmkf_id = false;
                bmkf_id = false;
            }
            token = strtok_r(NULL,s,&end_token);
         }
            
        if(bmkf_id && bmkf_path)
        {
            if(bmkf_cont)
            {
                NodoLD thisP= getMountedNode(mkf_id);
                char *dire = thisP.path;
                partition parAct = getPart(mkf_id);
                int startP = parAct.part_start;
                new_file(dire,startP,mkf_path,"contenido fijo del archivo a ver si escribe este es mucho mas largo para ver si esta creando mas inodos, especialmente si hace el indirecto por que no tengo idea de si esta corriendo o no, por favor funciona xs",mkf_p);
            }
            else
            {
                int i = 0;
                char contenido_abc[mkf_size];
                int cont_ascii = 97;
                for(i; i<mkf_size; i++)
                {
                    contenido_abc[i] = cont_ascii;
                    cont_ascii++;
                    if(cont_ascii == 123)
                    {
                        cont_ascii = 97;
                    }
                    
                }
                NodoLD thisP= getMountedNode(mkf_id);
                char *dire = thisP.path;
                partition parAct = getPart(mkf_id);
                int startP = parAct.part_start;
                new_file(dire,startP,mkf_path,contenido_abc,mkf_p);
                
            }
        }else{
            printf("Error en atributos mkfile\n");
        }
/*------------------------------------------------------------------------------------*/
/*-------------------------------------MKDIR------------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkdir",token)){
        char mkd_id[50];
        char mkd_path[256];
        int mkd_p = 0;
        
        bool bmkd_id = false;
        bool bmkd_path = false;
                
        token = strtok_r(NULL,s,&end_token);
    
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(mkd_id,atributo);
                    bmkd_id = true;
                }
            }else if(startsWith("path",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkd_path,atributo);
                bmkd_path = true;
            }else if(startsWith("p",token)){
                mkd_p = 1;
            }else{
                printf("Error, atributos extra en rmusr\n");
                bmkd_id = false;
                bmkd_id = false;
            }
            token = strtok_r(NULL,s,&end_token);
         }
            
        if(bmkd_id && bmkd_path){
                //-------------> accion mkfile 
            NodoLD thisP= getMountedNode(mkd_id);
            char *dire = thisP.path;
            partition parAct = getPart(mkd_id);
            int startP = parAct.part_start;
            new_directory(dire,startP,mkd_path,mkd_p);
        }else{
            printf("Error en atributos mkdir\n");
        }
/*------------------------------------------------------------------------------------*/
/*--------------------------------------LOGIN-----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("login",token)){
        char log_user[50];
        char log_pwd[50];
        char log_id[5];
        
        bool blog_user = false;
        bool blog_pwd = false;
        bool blog_id = false;
        
        token = strtok_r(NULL,s,&end_token);
        
         while( token != NULL ){
            char *atributo;
            char *end_atributo;
             
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(log_id,atributo);
                    blog_id = true;
                }
            }else if(startsWith("usr", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(log_user,atributo);
                blog_user = true;
            }else if(startsWith("pwd",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(log_pwd,atributo);
                blog_pwd = true;
            }else{
                printf("Atributos extra en Login\n");
                blog_id = false;
                blog_user = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
        
        if(blog_id && blog_user && blog_pwd){
            //-----------> accion de login
        }else{
            printf("Error en atributos login\n");
        }
/*------------------------------------------------------------------------------------*/
/*--------------------------------------LOGOUT----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("logout",token)){
        
        token = strtok_r(NULL,s,&end_token);
        if(token != NULL){
            printf("Error, atributos extra en logout\n");
        }else{
            //------------> accion logout
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------MKGRP----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkgrp",token)){
        char mkgrp_id[50];
        char mkgrp_nombre[50];
        
        bool bmkgrp_id = false;
        bool bmkgrp_nombre = false;
        
        token = strtok_r(NULL,s,&end_token);
        
         while( token != NULL ){
            char *atributo;
            char *end_atributo;
             
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(mkgrp_id,atributo);
                    bmkgrp_id = true;
                }
            }else if(startsWith("nombre",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkgrp_nombre,atributo);
                bmkgrp_nombre = true;
            }else{
                printf("Error, atributos extra en mkgrp\n");
                bmkgrp_id = false;
                bmkgrp_nombre = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
            
        if(bmkgrp_id && bmkgrp_nombre){
            //-------------> accion mkgrp
        }else{
            printf("Error en atributos mkgrp\n");
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------RMGRP----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("rmgrp",token)){
        char rmgrp_id[50];
        char rmgrp_nombre[50];
        
        bool brmgrp_id = false;
        bool brmgrp_nombre = false;
        
        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(rmgrp_id,atributo);
                    brmgrp_id = true;
                }
            }else if(startsWith("nombre",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(rmgrp_nombre,atributo);
                brmgrp_nombre = true;
            }else{
                printf("Error, atributos extra en rmgrp\n");
                brmgrp_id = false;
                brmgrp_nombre = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
            
        if(brmgrp_id && brmgrp_nombre){
            //-------------> accion rmgrp
        }else{
            printf("Error en atributos rmgrp\n");
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------MKUSR----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("mkusr",token)){
        char mkusr_id[50];
        char mkusr_usr[50];
        char mkusr_pwd[50];
        char mkusr_grp[50];
        
        bool bmkusr_id = false;
        bool bmkusr_usr = false;
        bool bmkusr_pwd = false;
        bool bmkusr_grp = false;
        
        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(mkusr_id,atributo);
                    bmkusr_id = true;
                }
            }else if(startsWith("usr",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkusr_usr,atributo);
                bmkusr_usr = true;
            }else if(startsWith("pwd",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkusr_pwd,atributo);
                bmkusr_pwd = true;
            }else if(startsWith("grp",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(mkusr_grp,atributo);
                bmkusr_grp = true;
            }else{
                printf("Error, atributos extra en mkusr\n");
                bmkusr_id = false;
                bmkusr_usr = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
            
        if(bmkusr_id && bmkusr_usr && bmkusr_pwd && bmkusr_grp){
            //-------------> accion mkusr
        }else{
            printf("Error en atributos mkusr\n");
        }
/*------------------------------------------------------------------------------------*/
/*--------------------------------------RMUSR-----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("rmusr",token)){
        char rmusr_id[50];
        char rmusr_usr[50];
        
        bool brmusr_id = false;
        bool brmusr_usr = false;
        
        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                if(idMountOk(atributo)){
                    strcpy(rmusr_id,atributo);
                    brmusr_id = true;
                }
            }else if(startsWith("usr",token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarUltimoEspacio(atributo);
                strcpy(rmusr_usr,atributo);
                brmusr_usr = true;
            }else{
                printf("Error, atributos extra en rmusr\n");
                brmusr_id = false;
                brmusr_usr = false;
            }
            token = strtok_r(NULL,s,&end_token);
        }
            
        if(brmusr_id && brmusr_usr){
            //-------------> accion rmgrp
        }else{
            printf("Error en atributos rmusr\n");
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------REP------------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("rep",token)){
        char rep_name[50];
        char rep_path[256];
        char rep_ruta[256];
        char rep_id[50];
        
        bool brep_id = false;
        bool brep_name = false;
        bool brep_path = false;

        token = strtok_r(NULL,s,&end_token);
        
        while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
            if(startsWith("id", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                strcpy(rep_id, atributo);
                quitarUltimoEspacio(rep_id);
                brep_id = true;
            }else if(startsWith("name", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                quitarEspacios(rep_name,atributo);
                strcpy(rep_name,atributo);
                quitarUltimoEspacio(rep_name);
                brep_name = true;
            }else if(startsWith("path", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                 int valretpath = limpiarPath(rep_path,atributo);
                 if(valretpath == 0){
                    brep_path =  true;
                 }
            }else if(startsWith("ruta", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                 int valretpath = limpiarPath(rep_ruta,atributo);
            }
            token = strtok_r(NULL,s,&end_token);
        }
        
        if(brep_id && brep_name && brep_path){
            NodoLD mountN= getMountedNode(rep_id);
            char *ruta = mountN.path;
            partition parAct = getPart(rep_id);
            int startPart = parAct.part_start;
            if(strcasecmp(rep_name, "mbr")==0)
            {
                repMBR(rep_id);
            }
            else if(strcasecmp(rep_name, "file")==0)
            {
                repDSK(rep_id, rep_path);
            }
            else if(strcasecmp(rep_name, "disk")==0)
            {
                //repBloques(rep_id,rep_path); 
            }
            else if(strcasecmp(rep_name, "inode")==0)
            {
                reportar_inodos(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "block")==0)
            {
                reportar_bloques(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "sb")==0)
            {
                reportar_sb(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "bm_arbdir")==0)
            {
                imprimir_bitmap_avd(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "bm_detdir")==0)
            {
                imprimir_bitmap_dd(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "bm_inode")==0)
            {
                imprimir_bitmap_inodos(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "bm_block")==0)
            {
                imprimir_bitmap_bloques(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "bitacora")==0)
            {
                // repBitMap(rep_id,rep_path);
            }
            else if(strcasecmp(rep_name, "directorio")==0)
            {
                reporte_avd(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "tree_file")==0)
            {
                reportar_file(ruta,startPart,rep_ruta);
            }
            else if(strcasecmp(rep_name, "tree_directorio")==0)
            {
                reportar_directorio(ruta,startPart,rep_ruta);
            }
            else if(strcasecmp(rep_name, "tree_complete")==0)
            {
                reporte_tree_complete(ruta,startPart);
            }
            else if(strcasecmp(rep_name, "ls")==0)
            {
                // repBitMap(rep_id,rep_path);
            }
        }else{
            printf("Faltan parametros en Rep\n");
        }
/*------------------------------------------------------------------------------------*/
/*---------------------------------------EXEC-----------------------------------------*/
/*------------------------------------------------------------------------------------*/
    }else if(startsWith("exec",token)){
        char exe_path[256];
        bool bexe_path = false;

        token = strtok_r(NULL,s,&end_token);
        
         while( token != NULL ){
            char *atributo;
            char *end_atributo;
            
             if(startsWith("path", token)){
                atributo = strtok_r(token,aptr,&end_atributo);
                atributo = strtok_r(NULL,aptr,&end_atributo);
                int valretpath = limpiarPath(exe_path,atributo);
                if(valretpath == 0){
                    bexe_path =  true;
                }
            }
            token = strtok_r(NULL,s,&end_token);
        }
         
        if(bexe_path){
            analyzeFile(exe_path); // exec &path->/home/leslie/Escritorio/archivo.txt
        }
    }
}



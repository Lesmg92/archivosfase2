
#*********************************************************************************************************
UnmoUnt &id1->vdb1
mouNt &NamE->PRI1  &path->"/home/archivos/fase 1/D1.dsk"
mouNt &NamE->LOG2  &path->"/home/archivos/fase 1/D1.dsk"
mouNt &NamE->PRI2  &path->"/home/archivos/fase 1/D3.dsk"

#Listar las particiones montadas
mouNt
#SAlida es la siguiente
#id->vda1 &path->"/home/archivos/fase 1/D1.dsk" &NamE->"PRI 1"
#id->vda2 &PAth->"/home/archivos/fase 1/D1.dsk" &name->"LOG 2"
#id->vdb1 &path->"/home/archivos/fase 1/D3.dsk" &name->"PRI 2"

MkfS &id->vda1 %type->fast %FS
MkfS &id->vda2 %type->full %FS->EC 
MkfS &id->vdb1 %type->fast %FS

#MANEJO DE ARCHIVOS

#CREANDO ARCHIVOS EN EL DISCO 1 PARTICION PRI 1

Mkfile &id->vda1 &name->olakase &contenido->"Ola k ase?"
Mkfile &id->vda1 &name->pcs &contenido->"-¿pero que haces tirando esos portátiles al río? -pero mira como beben los PC,s en el rio!"
Mkfile &id->vda1 &name->broma &contenido->"-Mi amor, estoy embarazada! ¿Que te gustaria que fuera? -Una broma :("
Mkfile &id->vda1 &name->gitano &contenido->"Un gitano con el ordenador: - ¿Ays qué pasa con el feisbu? Me dise su clave es incorresta, entonces pongo incorresta pero ¡no abre!!.."
Mkfile &id->vda1 &name->profesora &contenido->"Llega una profesora nueva al colegio y dice: -Si alguno se cree estupido que se levante!! Jaimito se levanta despacio de su silla y la profesora sorprendida le pregunta: -Jaimito tu piensas que eres estupido? -No profe, pero me daba mucha pena verla a usted sola ahi parada!!"
Mkfile &id->vda1 &name->google &contenido->"-Prométeme que cuidarás a los niños y que les hablarás de mí. -¿Ya has vuelto a poner tus síntomas en Google? -Te quiero mucho...."
Mkfile &id->vda1 &name->loremipsum &contenido->"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed turpis aliquet, suscipit dui quis, condimentum magna. Sed imperdiet bibendum interdum. Curabitur pharetra sapien consequat porta dignissim. Pellentesque vitae sapien eros. Nulla facilisi. Suspendisse potenti. Nulla eros orci, rutrum a convallis at, euismod nec nisi. Donec sed scelerisque erat. Sed nunc sapien, blandit et risus sed, imperdiet ullamcorper nibh. Phasellus nisl elit, consequat a tellus eu, porta ullamcorper ligula. Sed molestie velit eget fringilla imperdiet. Curabitur vel dui ac eros varius vestibulum et eleifend libero. Praesent ante lectus, posuere ut commodo gravida, ultricies id velit. Donec nisl dolor, tempus nec tortor eu egestas tincidunt nibh"
Mkfile &id->vda1 &name->binario &contenido->"solo existen 10 tipos de personas, las que saben binario y las que no"
Mkfile &id->vda1 &name->final &contenido->"Ola k ase?"


#CREANDO ARCHVOS EN EL DISCO 3 PARTICION PRI 2
Mkfile &id->vdb1 &name->olakase &contenido->"Ola k ase?"
Mkfile &id->vdb1 &name->pcs &contenido->"-¿pero que haces tirando esos portátiles al río? -pero mira como beben los PC,s en el rio!"
Mkfile &id->vdb1 &name->broma &contenido->"-Mi amor, estoy embarazada! ¿Que te gustaria que fuera? -Una broma :("
Mkfile &id->vdb1 &name->gitano &contenido->"Un gitano con el ordenador: - ¿Ays qué pasa con el feisbu? Me dise su clave es incorresta, entonces pongo incorresta pero ¡no abre!!.."
Mkfile &id->vdb1 &name->profesora &contenido->"Llega una profesora nueva al colegio y dice: -Si alguno se cree estupido que se levante!! Jaimito se levanta despacio de su silla y la profesora sorprendida le pregunta: -Jaimito tu piensas que eres estupido? -No profe, pero me daba mucha pena verla a usted sola ahi parada!!"
Mkfile &id->vdb1 &name->google &contenido->"-Prométeme que cuidarás a los niños y que les hablarás de mí. -¿Ya has vuelto a poner tus síntomas en Google? -Te quiero mucho...."
Mkfile &id->vdb1 &name->loremipsum &contenido->"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed turpis aliquet, suscipit dui quis, condimentum magna. Sed imperdiet bibendum interdum. Curabitur pharetra sapien consequat porta dignissim. Pellentesque vitae sapien eros. Nulla facilisi. Suspendisse potenti. Nulla eros orci, rutrum a convallis at, euismod nec nisi. Donec sed scelerisque erat. Sed nunc sapien, blandit et risus sed, imperdiet ullamcorper nibh. Phasellus nisl elit, consequat a tellus eu, porta ullamcorper ligula. Sed molestie velit eget fringilla imperdiet. Curabitur vel dui ac eros varius vestibulum et eleifend libero. Praesent ante lectus, posuere ut commodo gravida, ultricies id velit. Donec nisl dolor, tempus nec tortor eu egestas tincidunt nibh"
Mkfile &id->vdb1 &name->binario &contenido->"solo existen 10 tipos de personas, las que saben binario y las que no"
Mkfile &id->vdb1 &name->final &contenido->"Ola k ase?"

#CREANDO ARCHIVOS EN EL DISCO 1 PARTICION LOG 2
Mkfile &id->vda2 &name->olakase &contenido->"Ola k ase?"
Mkfile &id->vda2 &name->pcs &contenido->"-¿pero que haces tirando esos portátiles al río? -pero mira como beben los PC,s en el rio!"
Mkfile &id->vda2 &name->broma &contenido->"-Mi amor, estoy embarazada! ¿Que te gustaria que fuera? -Una broma :("
Mkfile &id->vda2 &name->gitano &contenido->"Un gitano con el ordenador: - ¿Ays qué pasa con el feisbu? Me dise su clave es incorresta, entonces pongo incorresta pero ¡no abre!!.."
Mkfile &id->vda2 &name->profesora &contenido->"Llega una profesora nueva al colegio y dice: -Si alguno se cree estupido que se levante!! Jaimito se levanta despacio de su silla y la profesora sorprendida le pregunta: -Jaimito tu piensas que eres estupido? -No profe, pero me daba mucha pena verla a usted sola ahi parada!!"
Mkfile &id->vda2 &name->google &contenido->"-Prométeme que cuidarás a los niños y que les hablarás de mí. -¿Ya has vuelto a poner tus síntomas en Google? -Te quiero mucho...."
Mkfile &id->vda2 &name->loremipsum &contenido->"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sed turpis aliquet, suscipit dui quis, condimentum magna. Sed imperdiet bibendum interdum. Curabitur pharetra sapien consequat porta dignissim. Pellentesque vitae sapien eros. Nulla facilisi. Suspendisse potenti. Nulla eros orci, rutrum a convallis at, euismod nec nisi. Donec sed scelerisque erat. Sed nunc sapien, blandit et risus sed, imperdiet ullamcorper nibh. Phasellus nisl elit, consequat a tellus eu, porta ullamcorper ligula. Sed molestie velit eget fringilla imperdiet. Curabitur vel dui ac eros varius vestibulum et eleifend libero. Praesent ante lectus, posuere ut commodo gravida, ultricies id velit. Donec nisl dolor, tempus nec tortor eu egestas tincidunt nibh"
Mkfile &id->vda2 &name->binario &contenido->"solo existen 10 tipos de personas, las que saben binario y las que no"
Mkfile &id->vda2 &name->final &contenido->"Ola k ase?"

#COMPROBANDO EL FUNCIONAMIENTO DEL PRIMER AJUSTE
Mkfile &id->vda1 &FILEID->binario &FILEID->gitano
#DEBERIA CREAR ESTE ARCHIVO JUSTO DESPUES DEL ARCHIVO 3
Mkfile &id->vda1 &name->binario &contenido->"solo existen 10 tipos de personas, las que saben binario y las que no"

#COMPROBANDO EL FUNCIONAMIENTO DEL PEOR AJUSTE
Mkfile &id->vda1 &FILEID-> olakase
#DEBERIA CREAR ESTE ARCHIVO JUSTO DESPUES DEL ARCHIVO 9
Mkfile &id->vda1 &name->binario &contenido->"solo existen 10 tipos de personas, las que saben binario y las que no"

#COMPROBANDO EL FUNCIONAMIENTO DEL MEJOR AJUSTE
Mkfile &id->vda1 &FILEID->binario &FILEID->gitano
#DEBERIA CREAR ESTE ARCHIVO JUSTO DESPUES DEL ARCHIVO 7
Mkfile &id->vda1 &name->binario &contenido->"archivo de un solo bloque"

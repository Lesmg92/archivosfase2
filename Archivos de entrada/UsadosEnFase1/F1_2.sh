
#Crear 12 particiones del mismo tamaño en el primer disco con ajustes diferentes
#Todas son de 10 Mb (La extendida es de 100 Mb)
#Tipo:		Primaria	Primaria	Extendida	Logica	Logica	Logica	Logica	Logica	Logica	Logica	Logica Primaria
#Ajuste:	Primer 		Mejor		&			Peor	Peor	Peor	Peor	Peor	Peor	Peor	Mejor   Peor
#Nombre:	PRI 1		PRI 2		EXT			LOG 1	LOG 2	LOG 3	LOG 4	LOG 5	LOG 6	LOG 7	LOG8   PRI 3
fdisk &Size->10 %UniT->M &path->"/home/archivos/fase 1/D1.dsk" \^
      %type->P %fit->FF &name->PRI1
fdisk &path->"/home/archivos/fase 1/D1.dsk" &Size->10000 \^
	  %fit->BF &name->PRI2
fdisk &path->"/home/archivos/fase 1/D1.dsk" %type->E &name->EXT &Size->100000
fdisk %type->L &Size->10000 %Unit->K &path->"/home/archivos/fase 1/D1.dsk" &name->LOG1
fdisK &name->LOG2 &Size->10000 &path->"/home/archivos/fase 1/D1.dsk" %type->L
fdisk %type->L &path->"/home/archivos/fase 1/D1.dsk" &name->LOG3 &Size->10000 %uniT->K
fdiSk %type->L &Size->10000 %unIt->K &PAth->"/home/archivos/fase 1/D1.dsk" &name->LOG4
fdisk %type->L &Size->10000 %unit->K &path->"/home/archivos/fase 1/D1.dsk" &name->LOG5
FdisK &Size->10000 &NamE->LOG6 %UNit->K &path->"/home/archivos/fase 1/D1.dsk" %type->L
fdIsk %type->L &siZe->10000 %unit->K &path->"/home/archivos/fase 1/D1.dsk" &name->LOG7
fdIsk %type->L &siZe->10000 %unit->K &path->"/home/archivos/fase 1/D1.dsk" &name->LOG8 %fit->BF
fdisk %unit->B &path->"/home/archivos/fase 1/D1.dsk" &Size->10000000 &name->PRI3

#Crearemos las mismas 12 particiones en el tercer disco
fdisk &Size->10 %UniT->M &path->"/home/archivos/fase 1/D3.dsk" \^
     %type->P %fit->FF &name->PRI1
fdisk &path->"/home/archivos/fase 1/D3.dsk" &Size->100000 \^
	 %fit->BF &name->PRI2
FdisK &path->"/home/archivos/fase 1/D3.dsk" %type->E &name->EXT &Size->100000
fdisk %type->L &Size->100000 %Unit->K &path->"/home/archivos/fase 1/D3.dsk" &name->LOG1
fdisK &name->LOG2 &Size->100000 &path->"/home/archivos/fase 1/D3.dsk" %type->L
fdisk %type->L &path->"/home/archivos/fase 1/D3.dsk" &name->LOG3 &Size->100000 %uniT->K
fdiSk &Size->100000 %unIt->K &PAth->"/home/archivos/fase 1/D3.dsk" &name->LOG4 %type->L
fdisk %type->L &Size->100000 %unit->K &path->"/home/archivos/fase 1/D3.dsk" &name->LOG5
FdisK &Size->100000 &NamE->LOG6 %UNit->K &path->"/home/archivos/fase 1/D3.dsk" %type->L
fdIsk %type->L &siZe->100000 %unit->K &path->"/home/archivos/fase 1/D3.dsk" &name->LOG7
fdIsk %type->L &siZe->100000 %unit->K &path->"/home/archivos/fase 1/D3.dsk" &name->LOG8 %fit->BF
fdisk %unit->B &path->"/home/archivos/fase 1/D3.dsk" &Size->10000000 &name->PRI3

#Verificar generacion de ids al montar
#Debería generar los ids: vda1, vdb1, vda2, vdb2
#vda para el d3.dsk y vdb para d1.dsk
mouNt &NamE->PRI3  &path->"/home/archivos/fase 1/D3.dsk"
moUnt &PAth->"/home/archivos/fase 1/D1.dsk" &name->PRI3
mOunt &name->PRI2 &path->"/home/archivos/fase 1/D3.dsk"
mouNt &path->"/home/archivos/fase 1/D1.dsk" &name->PRI2

#Validaciones al montar, no deberia cargarlos
moUnt &name->PX &path->"/home/archivos/fase 1/D3.dsk"
moUNt &path->"/home/archivos/fase 1/DX.dsk" &name->PRI3

#Listar las particiones montadas
mouNt
#SAlida es la siguiente
#id->vda1 &path->"/home/archivos/fase 1/D3.dsk" &NamE->"PRI 3"
#id->vdb1 &PAth->"/home/archivos/fase 1/D1.dsk" &name->"PRI 3"
#id->vda2 &path->"/home/archivos/fase 1/D3.dsk" &name->"PRI 2"
#id->vdb2 &path->"/home/archivos/fase 1/D1.dsk" &name->"PRI 2"

#Reporte de las particiones del disco
#Debe crear la carpeta de reportes
rep &name->"disk" &path->"/home/archivos/fase 1/reportes/Reporte1Disco3.jpg" &id->vda1
rep &path->"/home/archivos/fase 1/reportes/Reporte1Disco1.jpg" &id->vdb1 &name->disk


#Vericamos la eliminación
#Elimina la primaria 2, logica 1, logica 4 y logica 5 del disco 1
#Queda así: P1 EXT L2 L3 L6 L7 L8 P3
fdisk %delete->Fast &name->PRI2 &path->"/home/archivos/fase 1/D1.dsk"
fdisk &path->"/home/archivos/fase 1/D1.dsk" &name->LOG1 %delete->FasT
fDisk &name->LOG4 %delete->FaST &PAth->"/home/archivos/fase 1/D1.dsk"
fdisK &path->"/home/archivos/fase 1/D1.dsk" &NamE->LOG5 %delete->FAsT

#Tambien elimina la partición extendida y la primaria 1 del disco 3
#Verificar que no queden EBRS
#Queda así: P2 P3
fDisk &DELete->Full &Name->EXT &path->"/home/archivos/fase 1/D3.dsk" %Delete->Full
fdisk %Delete->Full &name->PRI1 %Delete->Full &path->"/home/archivos/fase 1/D3.dsk"

#Reporte con particiones eliminadas
rep &id->vda1 &name->disk &path->"/home/archivos/fase 1/reportes/Reporte2Disco3.jpg"
rep &path->"/home/archivos/fase 1/reportes/Reporte2Disco1.jpg" &name->disk &id->vdb1

#Teoria de particiones, (solo una extendida) debería dar error
fdisk &Size->2048 &path->"/home/archivos/fase 1/D1.dsk" %type->E &name->EXT2
fdisk &Size->2048 &path->"/home/archivos/fase 1/D3.dsk" %type->L &name->LOG78

#Verificar PRIMER AJUSTE EN PARTICIONES creando de nuevo las particiones
#Debe quedar de la siguiente forma:
#Disco1: P1 P2 EXT L1 L2 L3 L45 L6 L7 L8 P3
fdiSk %unit->M &Size->5 &path->"/home/archivos/fase 1/D1.dsk" %type->P %fit->FF &name->PRI2
fdisk &path->"/home/archivos/fase 1/D1.dsk" &Size->10240 %type->L &name->LOG45 %unit->K
fdisk &path->"/home/archivos/fase 1/D1.dsk" &Size->5120 %type->L &name->LOG1 %unit->K

#Disco 3: P2 EXT L1 P1 P3
fdisk %type->E &path->"/home/archivos/fase 1/D3.dsk" &Size->10240 &name->EXT
fdiSk &name->LOG1 %unit->K &path->"/home/archivos/fase 1/D3.dsk" %type->L &Size->5120
fdisk &Size->10240 &name->PRI1 &path->"/home/archivos/fase 1/D3.dsk" %type->P

#Reporte de las particiones del disco
Rep &Path->"/home/archivos/fase 1/reportes/Reporte 3 Disco3.jpg" &id->vda1 &name->disk
rep &name->disk &path->"/home/archivos/fase 1/reportes/Reporte 3 Disco2.jpg" &id->vdb1


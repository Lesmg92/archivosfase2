#Validaciones al crear o quitar espacio
#No deberia ejecutar estas instrucciones
#(porque no hay espacio después)
fdisk &path->"/home/archivos/fase 1/D1.dsk" &name->PRI1 %add->1
#(porque no debe quedar espacio negativo) debe mostrar mensaje de cuanto puede quitarle
fdisk %add->-20 %unit->M &path->"/home/archivos/fase 1/D1.dsk" &name->PRI1 %unit->M
#(Porque no hay suficiente espacio)
fdisk %unit->M &path->"/home/archivos/fase 1/D1.dsk" &name->LOG7 %add->15

#Quita y agrega 1 mb de la primer particion
fdisk %unit->M &path->"/home/archivos/fase 1/D1.dsk" &name->PRI1 %add->&1
rep &name->Mbr &path->"/home/archivos/fase 1/reportes/Reporte 1 MBR Disco 1.jpg" &id->vdb2
fdisk %unit->M &path->"/home/archivos/fase 1/D1.dsk" &name->PRI1 %add->1
rep &name->Mbr &path->"/home/archivos/fase 1/reportes/Reporte 2 MBR Disco 1.jpg" &id->vdb2
#Le quita 1 mb a la partición logica
fdisk %unit->M &path->"/home/archivos/fase 1/D1.dsk" &name->LOG45 %add->&1
rep &name->Mbr &path->"/home/archivos/fase 1/reportes/Reporte 3 MBR Disco 1.jpg" &id->vdb2

#desmontar una lista de ids
umount &id1->vda1 &id2->vdb2 &id->vda2
#para corroborar que desmonto hacer un mount para listar las particiones montadas
mount
#Salida es la siguiente
#id->vdb1 &PAth->"/home/archivos/fase 1/D1.dsk" &name->"PRI 3"

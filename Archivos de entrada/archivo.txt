#exec &path->/home/leslie/Escritorio/archivo.txt

#crearemos discos del mismo tamaño
mkdisk &size->512 &path->"/home/leslie/archivos/fase 2" &name->D1.dsk %unit->k
mkdisk &size->512 &path->"/home/leslie/archivos/fase 2" &name->D2.dsk %unit->k

rmdisk &path->"/home/leslie/archivos/fase 2/D2.dsk"

#vamos a verificar si hicieron validaciones con datos incorrectos
#primero el size tiene numero menor a 0, segudno el %unit tiene unidad desconocida, el tercero no es multiplo de 8
#MKdisk &SIze->-60 &path->"/home/leslie/archivos/fase 2" &name->D6.dsk
#MKdisk &SIze->60 %unit->Y &path->"/home/leslie/archivos/fase 2" &name->D7.dsk
#MKdisk &SIze->60 %unit->k &path->"/home/leslie/archivos/fase 2" &name->D8.dsk

#crear tres particiones primarias iguales
fdisk &size->128 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI1
fdisk &size->128 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI2
fdisk &size->128 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI3

#elimimnar segunda particion
fdisk &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI2 %delete->fast

#agreagar espacio a la primera particion
#fdisk &size->32 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI1 %add->32

#montar la primera particion
mount &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI1
mount &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI3

#mostrar particiones montadas
mount

#crear particion logica
#fdisk &size->16 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->LOG1 %type->l

#crear discos nuevos
mkdisk &size->512 &path->"/home/leslie/archivos/fase 2" &name->D3.dsk %unit->k
mkdisk &size->512 &path->"/home/leslie/archivos/fase 2" &name->D4.dsk %unit->k

#formatear particion 1
mkfs &id->vda1 &type->fast

#agregar archivo
mkfile &id->vda1 &path->/home/usr/otracosa.txt %p

#crear particion extendida y nueva primaria
#fdisk &size->128 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->EXT %type->e
#fdisk &size->32 &path->"/home/leslie/archivos/fase 2/D1.dsk" &name->PRI5


#reportar estado del disco
#rep &id->vda1 &path->"/home/leslie/archivos/fase 2/disco.png" &name->disk

#reportar estado del mbr
#rep &id->vda1 &path->"/home/leslie/archivos/fase 2/disco.png" &name->mbr


#ifndef ANALIZADOR_H
#define ANALIZADOR_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "GestorDiscos.h"
#include "SistemaA.h"

/*------------------------------------------------------------------------------------*/
/*---------------------------METODOS Y FUNCIONES AUXILIARES---------------------------*/
/*------------------------------------------------------------------------------------*/
bool startsWith(const char *pre, const char *str);
int buscarEspacios(char *token,char s);
void quitarEspacios(char *tmp, char *cadena);
void quitarUltimoEspacio(char str[256]);
void toLowerCase(char s[]);
void analyzeFile(char path[256]);
int verificarArchivoE(char path[300]);
void crearDirectorio(char path[300]);

/*------------------------------------------------------------------------------------*/
/*---------------------------VERIFICAR PARAMETROS CORRECTOS---------------------------*/
/*------------------------------------------------------------------------------------*/
bool nameDskOk(char s[]);
bool nameOk(char s[]);
int pathOk(char *str);
int limpiarPath(char pathRet[256], char atributo[256]);
bool unitOk(char car);
bool typeOk(char car);
char fitOk(char str[4]);
bool deleteOk(const char *str);
bool idMountOk(char *str);

/*------------------------------------------------------------------------------------*/
/*-------------------------------ACCIONES DE CADA COMANDO-----------------------------*/
/*------------------------------------------------------------------------------------*/
void orden(char str[256]);

#endif
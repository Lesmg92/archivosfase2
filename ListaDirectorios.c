#include "ListaDirectorios.h"

    listaDir *crearOrdenDirectorio(){
        listaDir *nuevaPila = malloc(sizeof(listaDir));
        nuevaPila->cabeza = NULL;
        return nuevaPila;
    }
    
    void insertar(char *nombre, listaDir *lista){
        nodoDir *nodoIn = malloc(sizeof(nodoDir));
        nodoIn->nombre = strdup(nombre);
        nodoIn->sig = NULL;

        nodoDir *nodoAMover = lista->cabeza;
        lista->cabeza = nodoIn;
        nodoIn->sig = nodoAMover;
    }

    char *getNodo(listaDir *lista){
        char *nombrer;
        if(lista->cabeza != NULL){
            nodoDir *anterior = NULL;
            nodoDir *nodoget = lista->cabeza;
            while(nodoget->sig != NULL)
                {
                    anterior = nodoget;
                    nodoget = nodoget->sig;
                }
   
            nombrer = strdup(nodoget->nombre);
            
            if(anterior != NULL)
                anterior->sig = NULL;
            
            if(nodoget == lista->cabeza)
                lista->cabeza = NULL;
                
            return nombrer;
        }
        return NULL;
    }
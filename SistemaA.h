#ifndef SISTEMAA_H
#define SISTEMAA_H
#include <time.h> 
#include <stdlib.h>

#include <strings.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>

#include "ListaDirectorios.h"

typedef struct superBoot{
    char sb_nombre_hd[16];
    unsigned int sb_arbol_virtual_count;
    unsigned int sb_detalle_directorio_count;
    unsigned int sb_inodos_count;
    unsigned int sb_bloques_count;
    unsigned int sb_arbol_virtual_free;
    unsigned int sb_detalle_directorio_free;
    unsigned int sb_inodos_free;
    unsigned int sb_bloques_free;
    time_t sb_date_creacion;
    time_t sb_date_ultimo_montaje;
    unsigned int sb_montajes_count;
    unsigned int sb_ap_bitmap_arbol_directorio;
    unsigned int sb_ap_arbol_directorio;
    unsigned int sb_ap_bitmap_detalle_directorio;
    unsigned int sb_ap_detalle_directorio;
    unsigned int sb_ap_bitmap_tabla_inodo;
    unsigned int sb_ap_tabla_inodo;
    unsigned int sb_ap_bitmap_bloques;
    unsigned int sb_ap_bloques;
    unsigned int sb_ap_log;
    unsigned int sb_size_struct_arbol_directorio;
    unsigned int sb_size_struct_detalle_directorio;
    unsigned int sb_size_struct_inodo;
    unsigned int sb_size_struct_bloque;
    unsigned int sb_first_free_bit_arbol_directorio;
    unsigned int sb_first_free_bit_detalle_directorio;
    unsigned int sb_first_free_bit_tabla_inodo;
    unsigned int sb_first_free_bit_bloque;
    unsigned int sb_magic_num;
}superBoot;

typedef struct arbolVirtualDirectorio{
   time_t avd_fecha_creacion;
   char avd_nombre_directorio[16];
   int avd_ap_array_subdirectorios[6];
   int avd_ap_detalle_directorio;
   int avd_ap_arbol_virtual_directorio;
   int avd_proper;
}arbolVirtualDirectorio;

typedef struct archivo{
    char dd_file_nombre[16];
    int dd_file_ap_inodo;
    time_t dd_file_date_creacion;
    time_t dd_file_date_modificacion;
}archivo;

typedef struct detalleDirectorio{
  archivo dd_archive_1,dd_archive_2,dd_archive_3,dd_archive_4,dd_archive_5;
  int dd_ap_detalle_directorio;
}detalleDirectorio;

typedef struct bloque{
  char dbdata[25];
}bloque;

typedef struct inodo{
   int i_count_inodo;
   int i_size_archivo;
   int i_count_bloques_asignados;
   int i_array_bloques[4];
   int i_ap_indirecto;
   int i_id_proper;
}inodo;

typedef struct Log{
    char log_tipo_operacion[40];
    int log_tipo;
    char log_nombre[40];
    char log_contenido[25];
    time_t log_fecha;
}Log;


typedef struct bitmapArbolDirectorio{
    char bmp_arbol_directorio[10000];
}bitmapArbolDirectorio;

typedef struct bitmapDetalleDirectorio{
    char bmp_detalle_directorio[10000];
}bitmapDetalleDirectorio;

typedef struct bitmapTablaINodos{
    char bmp_tabla_inodos[10000];
}bitmapTablaINodos;

typedef struct bitmapBloques{
    char bmp_bloques[10000];
}bitmapBloques;

// -----------------------------------
typedef struct existe_directorio{
   int existe;
   int numero_avd;
   int posicion_sub_avd;
}existe_directorio;


/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA FORMATEAR-------------------------*/
/*------------------------------------------------------------------------------------*/

int getNStructs(int partSize);
void formatPart(char ruta[256],char name[16],int partSize,int startPart);
int limitStructs(int numStru);

/*------------------------------------------------------------------------------------*/
/*------------------------GET Y SET PARA STRUCTS Y BITMAPS----------------------------*/
/*------------------------------------------------------------------------------------*/

superBoot getSuperBoot(char ruta[300], int startParticion);
void setSuperBoot(char ruta[300],superBoot sb1,int posicion);
arbolVirtualDirectorio getAVD(char ruta[300], int byte_avd);
void setAVD(char ruta[300],int byte_avd,arbolVirtualDirectorio avdadd);
detalleDirectorio getDetalleDirectorio(char ruta[300], int byte_dd);
void setDetalleDirectorio(char ruta[300],int byte_dd,detalleDirectorio ddAdd);
inodo getINodo(char ruta[300], int byte_inodo);
void setINodo(char ruta[300], int byte_inodo, inodo inodoAdd);
bloque getBloque(char ruta[300], int byte_bloque);
void setBloque(char ruta[300], int byte_bloque,bloque bloqAdd);

bitmapArbolDirectorio get_bm_avd(char ruta[300], int startPart);
void set_bm_avd(char ruta[300], int startPart, bitmapArbolDirectorio nadd);
bitmapDetalleDirectorio get_bm_dd(char ruta[300], int startPart);
void set_bm_dd(char ruta[300], int startPart, bitmapDetalleDirectorio nadd);
bitmapTablaINodos get_bm_tabla_in(char ruta[300], int startPart);
void set_bm_tabla_in(char ruta[300], int startPart, bitmapTablaINodos nadd);
bitmapBloques get_bm_bloques(char ruta[300], int startPart);
void set_bm_bloques(char ruta[300], int startPart,bitmapBloques nadd);

/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA INICIALIZAR-----------------------*/
/*------------------------------------------------------------------------------------*/

int new_AVD(char ruta[300], int startPart, char nombre[30]);
int new_detalle_directorio(char ruta[300],int startPart);
int new_inodo(char ruta[300],int startPart);
int new_bloque(char ruta[300],int startPart, char conte[25]);

/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA MODIFICAR-------------------------*/
/*------------------------------------------------------------------------------------*/

void agregarContEnBloques(int byte_in, char ruta[300], int startPart, char contenido[300], int cont_contenido, int cont_llenos);
void agregarArchivo(char ruta[300],int startPart, char contenido[300], char nombre[16], int from_avd_ap_dd);
void asignar_dd(char ruta[300],int startPart, int apt_dd, int byte_para_inodo, char nombre[16]);
void leerArchivo(char ruta[300],int startPart, char nombre[16]);
int buscar_inodo_nombre(char ruta[300],int startParticion,char name[25]);
int verifica_inodo_existente(char ruta[300],int startParticion,int num, char name[25]);
void getContBloques(char retCad[300], inodo p_inodo, char ruta[300]);

/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA DIRECTORIOS-----------------------*/
/*------------------------------------------------------------------------------------*/
void new_directory(char ruta[300],int startPart,char path[300],int p1);
int buscar_crear(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart);
int asignar_hijo_avd(char ruta[300], int byte_padre, int startPart, char nombre[16]);
int buscar_sin_crear(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart, char f_d);
int verificar_si_existe_avd(char ruta[300],int byte_inicio_avd,char nombre[16]);
int verificar_nombre_igual(char ruta[300],int ap_avdsub, char name[16]);
void new_file(char ruta[300],int startPart,char path[300],char contenido[256],int p1);
     
/*------------------------------------------------------------------------------------*/
/*-------------------------METODOS Y FUNCIONES PARA REPORTAR--------------------------*/
/*------------------------------------------------------------------------------------*/

void reporte_avd(char ruta[300], int startPart);
void recorrido_avd_subgraphs(char ruta[300], int byte_raiz, FILE *g);
void recorrido_avd_ptr(char ruta[300], int byte_raiz, FILE *g);
void recorrido_dd(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd);
void recorrido_dd_p_inodos(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd);
void recorrido_inodos(char ruta[300], FILE *g, inodo i_nodo,int ptrin,int ptrdd);
void recorrido_inodos_indirectos(char ruta[300], FILE *g,int ptrin,int ptrin2);
void reportar_inodos(char ruta[300], int startPart);
void reportar_rec_inodos(char ruta[300], int byte_ini, FILE *g);
void reportar_bloques(char ruta[300], int startPart);
void reportar_rec_bloque(char ruta[300], int byte_ini, FILE *g);
void reportar_sb(char ruta[300], int startPart);
void recorrer_avd(char ruta[300], int byte_raiz);
void imprimir_bitmap_dd(char ruta[300],int startPart);
void imprimir_bitmap_avd(char ruta[300],int startPart);
void imprimir_bitmap_inodos(char ruta[300],int startPart);
void imprimir_bitmap_bloques(char ruta[300],int startPart);
void reporte_tree_complete(char ruta[300], int startPart);
void recorrido_tc_subgraphs(char ruta[300], int byte_raiz, FILE *g);
void recorrido_tc_ptr(char ruta[300], int byte_raiz, FILE *g);
int graficar_por_nivel(listaDir *listaDirectorios,int byte_inicio_avd,char ruta[300], int startPart, FILE *g);
void reportar_file(char ruta[300], int startPart, char rutaSA[300]);
void graficar_archivo_en_dd(char ruta[300],FILE *g,detalleDirectorio dd,int ptrdd,char nombre[16]);


#endif
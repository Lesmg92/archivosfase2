#ifndef GESTORDISCOS_H
#define GESTORDISCOS_H

#include <strings.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <time.h> 
#include "montarpart.h"
#include "validaciones.h"
#include "SistemaA.h"

#define kb 1024 
#define mb 1048576 
typedef struct partition partition; 
typedef struct mbr mbr;
typedef struct ebr ebr;

/*------------------------------------------------------------------------------------*/
/*--------------------------ESTRUCTURAS PARA DISCO Y PARTICION------------------------*/
/*------------------------------------------------------------------------------------*/

struct partition
{
    char part_status;
    char part_type;
    char part_fit;
    int part_start;
    int part_size;
    char part_name[16];
};

struct mbr
{
    int mbr_tamano;
    time_t mbr_fecha_creacion;
    int mbr_disk_signature;
    struct partition mbr_partition_1;
    struct partition mbr_partition_2;
    struct partition mbr_partition_3;
    struct partition mbr_partition_4;
};

struct ebr
{
    int part_status;
    char part_fit;
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
};

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA GESTION DE DISCOS----------------------*/
/*------------------------------------------------------------------------------------*/
void crearDisco(char dir[256],int sizeDisk,char tipo);
void leerDisco(char dir[256]);
void eliminarDisco(char dir[256]);

/*------------------------------------------------------------------------------------*/
/*------------------METODOS Y FUNCIONES PARA GESTION DE PARTICIONES-------------------*/
/*------------------------------------------------------------------------------------*/
int getStartPart(const mbr mbr0, int sizeNPart, int pActual);
int particionLibre(char dir[256]);
int verificarExtendidaE(char dir[256]);
int VerificarParticionesExtendidas(char ruta[256]);
int InicioExtendidas(char ruta[256]);
void crearParticionLogica(char ruta[256], int x);
int TamanoExtendidas(char ruta[256]);
void ModificarEBR(char ruta[256],char fit,char name[16],int inicio,int next, int size, int status);
void LeerEBR(char ruta[256],int inicio);
int EnlazarEBR(char ruta[256],int inicio);
int TamDeEBR(char ruta[256],int inicio);
void crearParticionAux(char dir[256],char nom[16],char tipo, char fit, int sizePart);
void crearParticion(char dir[256],char nom[16],char tipo, char fit, int sizePart);
void modificarParticion(char dir[256],char nom[16],int modf);
void eliminarParticion(char dir[256],char nom[16]);

/*------------------------------------------------------------------------------------*/
/*-----------------------METODOS Y FUNCIONES PARA MOUNT-------------------------------*/
/*------------------------------------------------------------------------------------*/
int StartParticiones(char ruta[256], char name[300]);
void mountP(char path[300], char name[300]);
void unmountP(char name[300]);
partition auxGetPart(char ruta[256], char name[300]);
partition getPart(char namePart[100]);

/*------------------------------------------------------------------------------------*/
/*--------------------METODOS Y FUNCIONES PARA REPORTAR ARCHIVOS----------------------*/
/*------------------------------------------------------------------------------------*/
void repDSK(char id[50], char ruta[256]);
int next1(char ruta[256],int inicio);
int size1(char ruta[256],int inicio);
void name21(char ruta[256],int inicio, char ret[16]);
void repMBR(char dir[256]);

#endif